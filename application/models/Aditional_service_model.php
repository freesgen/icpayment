<?php

class Aditional_service_model extends CI_model
{
	public function add($serviceId, $contractId)
	{
		$result = $this->db->insert('ic_servicios_adicionales', [
		'id_servicio_adicional' => null,
		'id_servicio' => $serviceId,
		'id_contrato' => $contractId
	  ]);

		if ($result) {
			$aditionalServiceId = $this->db->insert_id();
			return $this->get($aditionalServiceId);
    }
    var_dump($this->db->last_query());
		return false;
	}

	public function get($aditionalServiceId)
	{
		$this->db->where('id_servicio_adicional', $aditionalServiceId);
		if ($extraService = $this->get_view()) {
			return $extraService->row_array();
		}
		return false;
	}

	public function get_of_contract($contractId)
	{
		$this->db->where('id_contrato', $contractId);
		if ($extraService = $this->get_view()) {
			return $extraService->result_array();
		}
		return false;
	}

	public function delete($aditionalServiceId)
	{
		if ($this->db->delete('ic_servicios_adicionales', ['id_servicio_adicional' => $aditionalServiceId])) {
			return true;
		}
		return false;
	}

	public function get_view()
	{
		return $this->db->select('ic_servicios_adicionales.* ,ic_servicios.*', false)
				->join('ic_servicios', 'ic_servicios_adicionales.id_servicio = ic_servicios.id_servicio', 'LEFT')
				->get('ic_servicios_adicionales');
	}
}
