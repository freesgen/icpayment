<?php 
  class Pusher extends Pusher\Pusher {

    public function __construct(){
      parent::__construct(
        getenv('PUSHER_KEY'),
        getenv('PUSHER_SECRET'),
        getenv('PUSHER_ID')
      );
    }
  
  }