import Vue from "vue";
import "element-ui/lib/theme-chalk/index.css";

import {
    DatePicker,
    Card,
    Col,
    Row,
    Dialog,
    Button,
    Table,
    TableColumn
  } from "element-ui";
  import locale from 'element-ui/lib/locale'
  import lang from "element-ui/lib/locale/lang/es";
  
  window.EventBus = Event;
  Vue.use(lang)
  Vue.use(DatePicker)
  Vue.use(Card)
  Vue.use(Col)
  Vue.use(Row)
  Vue.use(Dialog)
  Vue.use(Table)
  Vue.use(TableColumn)
  Vue.use(Button)