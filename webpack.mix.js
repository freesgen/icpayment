
const mix = require('laravel-mix');
const esLintConfig = require('./.eslintrc');
const path = 'src/';
const webpack = require("webpack");

const Libraries = [
  path + "js/lib/axios.min.js",
  // path + "js/lib/bootstrap.min.js",
	path + "bt/bootstrap-table.min.js",
  path + "bt/locale/bootstrap-table-es-SP.min.js",
  path + "js/lib/sweetalert2.min.js",
  path + "js/lib/jquery.inputmask.js",
	path + "js/lib/icheck.min.js",
  path + "js/lib/peace.min.js",
  path + "js/lib/select2.full.min.js"
]
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.babel('src/js/oldComponents/','assets/js/bundle/components.min.js')
mix.combine(Libraries,'assets/js/bundle/libraries.min.js')

mix.js('src/js/main.js', 'assets/js/bundle/')
		.js('src/js/utils/libs.js', 'assets/js/bundle/')
    // .eslint(esLintConfig)
    .setPublicPath('assets')
    .extract(['vue', 'jquery']);
    
    // Sass
mix.js('src/js/components/login', 'assets/js/bundle/login.js')  
mix.sass('src/assets/css/main.sass', 'assets/css/mainv2.css')

mix.webpackConfig({
  module: {
    rules: [
      {
        test: /\.pug$/,
        oneOf: [
          {
            resourceQuery: /^\?vue/,
            use: ['pug-plain-loader']
          },
          {
            use: ['raw-loader', 'pug-plain-loader']
          }
        ]
      }
    ],
  },
      plugins: [
        new webpack.NormalModuleReplacementPlugin(/element-ui[\/\\]lib[\/\\]locale[\/\\]lang[\/\\]zh-CN/, 'element-ui/lib/locale/lang/es')
      ]

});

mix.options({
  extractVueStyles: 'assets/css/vue-styles.css', // Extract .vue component styling to file, rather than inline.
  globalVueStyles: 'src/scss/1-base/_colors.sass', // Variables file to be imported in every component.
  processCssUrls: false, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
  purifyCss: false, // Remove unused CSS selectors.
  uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
  postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
});

mix.browserSync({
  proxy: 'http://icpayment.test'
});
