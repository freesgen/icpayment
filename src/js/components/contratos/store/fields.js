export default [{
    name: 'fecha',
    title: 'Fecha',
  }, {
    name: 'cliente',
    title: 'Cliente',
    filterable: true,
    customDisplay(row, field, store) {
        if (field) {
        return `${field.nombres} ${field.apellidos}`
        }
        return ``
    }
  },
  {
    name: 'estado',
    title: 'Grupo',
    filterable: true
  },
  {
    name: 'deuda_contrato',
    title: 'Identificador',
    filterable: false,
    row_class: 'no'
  }, {
    name: 'cuotas',
    title: 'Sucursal',
  }, {
    name: 'contrato',
    title: 'Documento',
    filterable: true,customDisplay(row, field, store) {
        if (field) {
        return ``
        }
        return ``
    }
  },
];