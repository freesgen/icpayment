export function confirmDelete(title, text, confirmButtonText, cancelButtonText) {
  return swal({
    title: title || 'Está Seguro?',
    text: text || "Seguro que desea eliminar el seguro a este contrato?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: confirmButtonText || 'Eliminar',
    cancelButtonText: cancelButtonText || 'Cancelar'
  })
}

export function createFormData(data) {
  return `data=${JSON.stringify(data)}`;
}
