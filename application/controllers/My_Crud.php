<?php
namespace freesgen\application\controllers;
use \Carbon\Carbon;
use freesgen\application\libraries\MyPusher;
use freesgen\application\listeners\PaymentAgreement as PaymentAgreementListener;

defined('BASEPATH') OR exit('No direct script access allowed');

class My_Crud extends \MY_Controller {

	protected $model;
	protected $listener;
	protected $fields;
	protected $emitter;
	protected $modelName;
	protected $events = [
		'beforeGet',
		'afterGet',
		'beforeCreate',
		'afterCreate',
		'beforeUpdate',
		'afterUpdate',
		'beforeDelete',
		'afterDelete',
	];
	private $pusher;

	public function __construct() {
		parent::__construct();
		$this->emitter = new \League\Event\Emitter;
		$this->model = new $this->model;
		$this->load->helper('url');
		if ($this->listener) {
				$this->listener = new $this->listener;
				$this->pusher = new MyPusher();
				foreach ($this->events as $event) {
					$this->emitter->addListener("$this->modelName::$event", $this->listener);
				}	
		}
		$this->my_auth->authenticate();
	}

	public function index($id = null, $type = 'response', $params = null) {
		$queryParams   = $_SERVER['QUERY_STRING'];
		$queryParams = $this->getParams($queryParams);
		
		if (!$id) {
			$modelQuery = $this->model->select();
			$count = $this->getModelQuery($modelQuery, $queryParams);

			$results = $modelQuery->get()->toArray();

			$returnData = [
					'data' => $results,
					'count' => $count,
			];
		} else {
			$modelQuery = $this->model->find($id);
			$this->getSingleQuery($modelQuery, $queryParams);
			$results = $modelQuery;
			$returnData = [
					'data' => $results,
			];
		}
		
		if ($this->listener) {
			$this->emitter->emit("$this->modelName::afterGet", $results);
		}

		if ($type != 'response') {
			return $results;
		}
		
		return $this->response_json($returnData);
	}

	public function create() {
		
		$data = $this->get_post_data('data');
		if ($data) {
			$resource = $this->model::create($data);
			$returnData = [
					"message" => $this->createdMessage,
					"data" => $resource
			];
		}

		if ($this->listener) {
			$this->emitter->emit("$this->modelName::*", $resource);
		}

		return $this->response_json($returnData);
	}

	public function edit($id) {

	}

	public function destroy($id) {

	}

	protected function getParams($queryParams) {
		if ($queryParams) {
			$queryParams = explode('&', $queryParams);
			$results = [];
	
			foreach ($queryParams as $param) {
				$param = explode('=', $param);
				$results[$param[0]] = $param[1];
			}
	
			return $results;
		}

		return [];
	}

	protected function getModelQuery($modelQuery, $queryParams) {
		$page = isset($queryParams['page']) ?  $queryParams['page']: 1;
		$limit = isset($queryParams['limit']) ?  $queryParams['limit']: 50;
		$query = isset($queryParams['query']) ?  $queryParams['query']: '';
		$ascending = isset($queryParams['ascending']) ?  $queryParams['ascending']: '';
		$byColumn = isset($queryParams['byColumn']) ?  $queryParams['byColumn']: '';
		$orderBy = isset($queryParams['orderBy']) ?  $queryParams['orderBy']: null;
		$filters = isset($queryParams['filters']) ?  $queryParams['filters']: '';
		
		if (isset($queryParams['relationships'])) {
			$relationships = $this->getRelationships($queryParams['relationships']);
		}

		if (isset($query) && $query) {
				$modelQuery = $byColumn == 1 ?
						$this->filterByColumn($modelQuery, $query) :
						$this->filter($modelQuery, $query, explode(',', $filters));
		}

		$count = $modelQuery->count();

		$modelQuery->limit($limit)
				->skip($limit * ($page - 1));

		if (isset($orderBy)) {
				$direction = $ascending == 1 ? 'ASC' : 'DESC';
				$modelQuery->orderBy($orderBy, $direction);
		}
		
    if (isset($relationships) && \is_array($relationships)) {
      forEach($relationships as $relation)  {
        $modelQuery->with($relation);
      }
	}
		
		return $count;
  }

	protected function getSingleQuery($modelQuery, $queryParams) {
		if (isset($queryParams['relationships'])) {
			$relationships = $this->getRelationships($queryParams['relationships']);
		}

		if (isset($relationships) && \is_array($relationships)) {
      $modelQuery->load($relationships);
		}
	}
	
  /**
   * get all the ralationships in the query
   * @param {String} relationships 
   * @return Array
   */
  protected function getRelationships($relationships) {
    if ($relationships) {
      return explode(',', $relationships);
    }
  }

	protected function filterByColumn($data, $queries)
	{
			return $data->where(function ($q) use ($queries) {
					foreach ($queries as $field => $query) {
							if (is_string($query)) {
									$q->where($field, 'LIKE', "%{$query}%");
							} else {
									$start = Carbon::createFromFormat('Y-m-d', $query['start'])->startOfDay();
									$end = Carbon::createFromFormat('Y-m-d', $query['end'])->endOfDay();

									$q->whereBetween($field, [$start, $end]);
							}
					}
			});
	}

	protected function filter($data, $query, $fields)
	{
			return $data->where(function ($q) use ($query, $fields) {
					foreach ($fields as $index => $field) {
							$method = $index ? 'orWhere' : 'where';
							$q->{$method}($field, 'LIKE', "%{$query}%");
					}
			});
	}
}
