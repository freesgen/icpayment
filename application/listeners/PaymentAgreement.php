<?php
namespace freesgen\application\listeners;
use League\Event\EventInterface;
use freesgen\application\listeners\ListenerBase;

class PaymentAgreement extends ListenerBase
{
    protected $listenerName = 'paymentAgreement';

    public function afterGet(EventInterface $event, $param = null) {
        $this->pusher->trigger('my-channel', 'global', 'hello');
    }
}