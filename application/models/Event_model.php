<?php
/**
* IC Payment
*@author Jesus Guerrero
*@copyright Copyright (c) 2017 Insane Code
*@version 1.0.0
*
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Event_model extends CI_MODEL{
  private $table;
  public function __construct() {
    parent::__construct();
  }

  public function add($event) {
    return $this->db->insert('ic_eventos', $event);
  }

  public function get($text = null, $first_date = null, $second_date = null, $json = false) {
    $first_date  = ($first_date)  ? $first_date  : '2001-01-01';
    $second_date = ($second_date) ? $second_date : date('Y-m-d');
		$where = "fecha between '$first_date' and '$second_date'";
		$base = base_url();

		$this->db->select("ic_eventos.*, concat(ic_users.name, ' ', ic_users.lastname) as autor,
		concat('$base', ic_eventos.enlace) as link", false);
    $this->db->order_by("fecha DESC, id_evento DESC");
    $this->db->where($where,'',false);
    $this->db->like('descripcion', $text);
    $this->db->or_like("concat(ic_users.name, ' ', ic_users.lastname)", $text);
    $this->db->join('ic_users','ic_eventos.id_usuario = ic_users.user_id', 'left');

    if ($result = $this->db->get('ic_eventos')) {
      $result = $result->result_array();
      $_SESSION['last_event_result'] = $result;

      if ($json) {
        return $result;
      } else {
        $fields = [
          table_field('id_evento'),
          table_field('fecha','','date'),
          table_field('autor'),
          table_field('tipo'),
          table_field('descripcion'),
          table_field('titulo_enlace'),
          table_field('link')
        ];
        return ['content' => make_simple_table($result, 0, $fields)];
      }
    }

  }

  public function get_excel_data($automatic = false) {

    $data = $automatic ? $this->get(null, null, null, true): $_SESSION['last_event_result'];
    $final_data = [];

    foreach($data as $line) {
      array_push($final_data, [
        $line['id_evento'],
        $line['fecha'],
        $line['autor'],
        $line['tipo'],
        $line['descripcion'],
        $line['titulo_enlace'],
        "=Hyperlink(\"{$line['link']}\", \"ver {$line['titulo_enlace']}\")"
      ]);
    }

    return [
      'header'    => ['Id Evento','Fecha','Autor','Accion','Descripcion', 'Seccion', 'Enlace(Puede dar click)'],
      'dimensions'=> [50,100,50,100],
      'data'      => $final_data,
      'start'     =>'B5',
      'end'       => 'H5'
    ];
  }

  public function free_space($months, $date = 'CURDATE()') {
    $this->db->where("period_diff(DATE_FORMAT('$date', '%Y%m'), DATE_FORMAT(fecha, '%Y%m')) >= $months", null, false);
    if ($this->db->delete('ic_eventos')) {
      return true;
    }
  }

}
