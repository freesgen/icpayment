window.sectionTable = {
  init: function(){
    var self = this;
    this.el = $('#t-sections');
    this.$filter = $('.filter');
    this.el.bootstrapTable();
    this.customSearch();
    this.el.find('tbody').css({display:"table-row-group"});
    this.el.addClass('innertable');
    this.detectClicks();
    this.el.on('all.bs.table', function (name, param) {
      self.loadingEvents();
   });
  },

  getSelectedRow: function(){
    var self = this;
    return self.el.bootstrapTable('getSelections')[0]
  },

  getId: function(){
    var self = this;
    const id = $.map(self.el.bootstrapTable('getSelections'),function(row){
      return row.id;
    });

    return id.slice(-1)
  },

  refresh: function(content,callback){
    sectionTable.el.bootstrapTable('destroy');
    sectionTable.el.find('tbody').html(content);
    sectionTable.init();
    if(callback) callback();
  },

  updateCell: function(uniqueId, cellName, value, className) {
    var data = sectionTable.el.bootstrapTable('getData');
    var sel = sectionTable.el.bootstrapTable('getRowByUniqueId','40/2');
    var i = data.indexOf(sel);
    this.el.bootstrapTable('updateCell',{index: i, field: cellName, value: value})
  },

  loadingEvents: function() {
    this.el.on('pre-body.bs.table',function () {
       //TODO: LOAD EVENTS
    });
    this.el.on('post-body.bs.table',function () {
      //TODO: LOAD EVENTS
    });
  },

  detectClicks: function(){
    this.el.on('check.bs.table',function(){
      var row= sectionTable.getSelectedRow();
    });

    this.el.on('uncheck.bs.table',function(){
     // TODO: check events
    });
  },

  customSearch: function () {
    $('.pull-right.search').addClass('hide')
    console.log('here is the custom search')
    var $inputSearch = $('.search input');
    this.$filter = $('.filter');
    var self = this

    $inputSearch.on('click', function (e) {
      var $this = $(this).parent();
      $this.addClass('focus')
    });

    $inputSearch.on('blur', function (e) {
      var $this = $(this).parent();
      $this.removeClass('focus');
    });

    this.$filter.on('change', function (e) {
      var _filtro = $(this).val();
      _filtro = _filtro.split(' ');
      self.applyFilter(_filtro);
      console.log(_filtro);
    })
  },

  applyFilter: function(filter) {
    this.el.bootstrapTable('filterBy',{
      estado: filter
    })
  }
}
