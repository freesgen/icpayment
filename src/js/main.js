import './lib/globals';
import './utils/base';
import './views/tables';
import './services';
import './views/ajax';
import './views/ajax2';

import * as Utils from './utils/Utils';

import ContractDetail from './components/shared/ContractDetail';
import ExtraServiceItem from './components/shared/ExtraServiceItem';
import PriceOptionsView from './components/services/PriceOptionsView';
import ServicePrices from './components/services/ServicePrices';
import './index.js';

// import GraphicReportSection from './components/reportes/GraphicReportSection.vue';

// Vue.component('graphic-report', GraphicReportSection);
Vue.component('extra-service-item', ExtraServiceItem);
Vue.component('contract-detail-section', ContractDetail);
Vue.component('service-prices', ServicePrices);
Vue.component('price-options-view', PriceOptionsView);

try {
  new Vue({
    el: '#add-extra-modal',
    data(){
      return {
        aditionalServices: [],
        contractId: ''
      }
    },
    watch: {
      contractId() {
        this.fetchAditionals();
      }
    },
    mounted() {
      this.fetchAditionals();
      bus.$on('extra-contract-selected', contractId =>  {
        contractId = Array.isArray(contractId) ? contractId[0] : contractId;
        this.contractId = contractId;
      })

      bus.$on('aditional-service-added', this.fetchAditionals);
    },
    methods: {
      fetchAditionals() {
        if (this.contractId) {
          axios.get(`/aditional/get/${this.contractId}`)
          .then(res => {
            console.log(res.data);
            this.aditionalServices = res.data;
          });
        }
      },

      removeAditional(id) {
        Utils.confirmDelete(null, 'Seguro de que desea eliminar el servicio adicional?')
          .then(() => {
            axios.delete(`/aditional/delete/${id}`)
            .then((res) => {
              displayMessage(res.data.message);
              this.fetchAditionals();
            })
            .catch((error) => {
              console.log(error);
            });

          });
      }
    }
  })
} catch(e) {

}
