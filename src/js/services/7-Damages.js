window.Damages = {

  add: function (idCliente, idContrato) {
    var form, description;
    description = $("#a-description").val();

    var is_empty = isEmpty([idCliente, description]);
    if (!is_empty) {
      form = 'id_cliente=' + idCliente + "&descripcion=" + description + "&id_contrato="+idContrato+"&tabla=averias";
      connectAndSend("process/add", true, initGlobalHandlers, null, form, Damages.getAll);
    } else {
       displayAlert("Revise", "LLene todos los campos por favor", "error");
    }
    $('#new-averia-modal').find('input,textarea').val("");
  },

  getAll: function () {
    var status = $("#averias-view-mode").val();
    $(".presentado").text(status);
    var form = "tabla=averias&estado=" + status;
    connectAndSend('process/getall', false, initGlobalHandlers, fillAveriasList, form, null);
  },

  update: function (idAveria) {
    var form = "tabla=averias&id_averia=" + idAveria;
    connectAndSend('process/update', true, initGlobalHandlers, null, form, Damages.getAll);
  },

  getContracts: function(idCliente) {
    return axios.get(BASE_URL + 'contract/get_contracts/' + idCliente)
    .then(function(res){
      return res.data;
    })
    .catch(function(err){ console.log(err)})
  }

}
