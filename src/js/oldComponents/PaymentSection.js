if (isCurrentPage("detalles")) {
  (function(){

    var appPaymentSection = new Vue({
      el: "#payment-controls",
      data: {
        paymentIds: [],
        print_mode: false,
        paid: "✔",
        showReceipt: false
      },

      computed: {
        linkReceipt: function() {
          if (this.paymentIds.length > 0) {
            var params = paymentTable.getIdSelections().join('_');
            return BASE_URL + 'app/imprimir/recibo/' + params;
          }
          return '';
        },

        btnClass: function () {
           return this.print_mode ? 'btn-danger' : 'btn-primary';
        },

        modeText: function () {
          return this.print_mode ? 'Modo Normal': 'Recibo Multiple';
        }
      },

      mounted() {
        var self = this;
        bus.$on('row-payment-selected', function (row) {
          self.checkPayments();
        })
        initPaymentsHandlers();
      },

      methods:{
        toggleMode: function () {
          this.changeMode();
          if (this.print_mode) {
            paymentTable.destroy()
            paymentTable.init(null, null, { singleSelect: false })
            paymentTable.filter(this.paid, 'estado')
          } else {
            paymentTable.destroy()
            paymentTable.init()
            Payments.getAll();
            this.clear();
          }
        },

        changeMode: function () {
          this.print_mode = !this.print_mode;
        },

        checkPayments: function() {
          this.paymentIds = paymentTable.getIdSelections() || [];
          this.showReceipt = (this.paymentIds.length > 0)
        },

        clear: function() {
          this.print_mode = false;
          this.paymentIds = [];
          this.showReceipt = false;
        }
      }
    });
  })()
}
