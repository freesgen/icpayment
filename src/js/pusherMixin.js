export default {
    data() {
        return {
            icChanel: null,
        }
    },

    mounted() {
        this.subscribeTochannel();
    },

    methods: {
        subscribeTochannel() {
            this.icChanel = this.$pusher.subscribe('my-channel');
            this.icChanel.bind('global', (data) => {
                console.log(data)
            })
        }
    }
}