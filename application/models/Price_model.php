<?php

class Price_model extends CI_model
{
	public function add($data)
	{
		$result = $this->db->insert('ic_precios', [
      'id_precio'   => null,
      'id_servicio' => $data['id_servicio'],
      'descripcion' => $data['descripcion'],
      'monto'       => $data['monto'],
      'activo'      => false
	  ]);

		if ($result) {
			$priceId = $this->db->insert_id();
			return $this->get($priceId);
    }

    echo $this->db->last_query();
		return false;
	}

	public function get($priceId)
	{
		$this->db->where('id_precio', $priceId);
		if ($price = $this->get_view()) {
			return $price->row_array();
		}
		return false;
	}

	public function get_of_service($serviceId)
	{
		$this->db->where('id_servicio', $serviceId);
		if ($price = $this->get_view()) {
			return $price->result_array();
		}
		return false;
  }

  public function update($priceId, $data) {
    $price = $this->get($priceId);
    $this->db->where('id_precio', $priceId);
    if ($this->db->update('ic_precios', $data)){
			  echo MESSAGE_SUCCESS." Servicio Actualizado Con Exito!";
			if ($data['monto'] == $price['monto']) {
				return true;
      } else {
        return update_contract_from_price($data);
      }
    } else {
      echo MESSAGE_ERROR." No pudo actualizarse el precio";
    }
  }

	public function delete($priceId)
	{
		if ($this->db->delete('ic_precios', ['id_precio' => $priceId])) {
			return true;
		}
		return false;
	}

	public function get_view()
	{
		// return $this->db->select('ic_servicios_adicionales.* ,ic_servicios.*', false)
		// ->join('ic_servicios', 'ic_servicios_adicionales.id_servicio = ic_servicios.id_servicio', 'LEFT')
		return $this->db->get('ic_precios');
	}
}
