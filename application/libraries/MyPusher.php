<?php 
  namespace freesgen\application\libraries;

  class MyPusher extends \Pusher\Pusher {

    public function __construct(){
      parent::__construct(
        getenv('PUSHER_KEY'),
        getenv('PUSHER_SECRET'),
        getenv('PUSHER_ID'),
        [
          'cluster' => 'us2',
          'useTLS' => true
        ]
      );
    }
  
  }