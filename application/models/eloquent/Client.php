<?php
namespace freesgen\application\models\eloquent;

class Client extends \Illuminate\Database\Eloquent\Model
{
    //
    protected $table = 'ic_clientes';
    protected $guarded = ['created_at', 'updated_at'];
}