<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aditional extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('aditional_service_model');
	}

	public function get($id, $mode = 'contract')
	{
		authenticate();
		if ($mode === 'contract') {
			$res = $this->aditional_service_model->get_of_contract($id);
		} else {
			$res = $this->aditional_service_model->get($id);
		}
		echo json_encode($res);
	}

	public function create()
	{
		authenticate();
		$data = json_decode($_POST['data'], true);
		$res['recibo'] = $this->payment_model->get_payment($data['id_pago']);
		echo json_encode($res);
	}

	public function delete($aditionalServiceId)
	{
		authenticate();
		if ($aditionalServiceId) {
			$result = $this->aditional_service_model->delete($aditionalServiceId);
			if ($result) {
				$this->response_json(['status' => 200, 'message' => 'Servicio adicional eliminado']);
			} else {
				$this->response_json(['status' => 400, 'message' => 'el servicio adicional no pudo ser eliminado']);
			}
		}
	}

	public function update($aditionalServiceId)
	{
		authenticate();
		$data = json_decode($_POST['data'], true);
		$info = json_decode($_POST['info'], true);
		if (!$this->payment_model->check_for_update($info['id_pago']) && $data) {
			if ($this->extra_model->apply_payment($data, $info)) {
				// event
				$receipt = $this->payment_model->get_payment($info['id_pago'], true);
				$this->event->trigger('payment', 2, $receipt, 'pago de servicio extra');
			}
		}
	}
}
