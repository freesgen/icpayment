<?php
namespace freesgen\application\models\eloquent;

class Payment extends \Illuminate\Database\Eloquent\Model
{
        //
        protected $table = 'ic_pagos';
        public $timestamps = false;
        protected $guarded = ['complete_date'];
        protected $primaryKey = "id_pago";

        public function contract()
        {
            return $this->belongsTo('freesgen\application\models\eloquent\Contract', 'id_contrato', 'id_contrato');
        }
        public function user()
        {
            return $this->belongsTo('freesgen\application\models\eloquent\User', 'user_id', 'user_id');
        }
        public function service()
        {
            return $this->belongsTo('freesgen\application\models\eloquent\Service', 'id_servicio', 'id_servicio');
        }
        public function price()
        {
            return $this->belongsTo('freesgen\application\models\eloquent\Price', 'id_precio', 'id_precio');
        }
        public function client()
        {
            return $this->belongsTo('freesgen\application\models\eloquent\Client', 'id_cliente', 'id_cliente');
        }

        public function get_extras($id_pago, $get_values = false)
        {
            $pago = $this::find($id_pago);
            if ($pago->servicios_adicionales) {
                $extras = json_decode($pago->servicios_adicionales, 1);
                if (!$get_values) {
                    return $extras;
                } else {
                    $total_extras = 0;
                    $string_detalles = '';
                    $detalles_extras = [];
    
                    foreach ($extras as $key => $extra) {
                        $total_extras += $extra['precio'];
                        $string_detalles .= "{$extra['servicio']} -- ";
                        array_push($detalles_extras, $extra);
                    }
    
                    return ['total' => $total_extras, 'detalles' => $string_detalles, 'array' => $detalles_extras];
                }
            }
        }

        public function set_extra($new_extra, $id_pago)
        {
            $extras = $this->get_extras($id_pago);
            if (!$extras) {
                $extras = $new_extra;
            } else {
                $key = join('', array_keys($new_extra));
                $extras[$key] = $new_extra[$key];
            }
            return $this->save_extras($extras, $id_pago);
        }

        public function save_extras($extras, $id_pago)
        {
            $extras = json_encode($extras);
            return $this->update(['servicios_adicionales' => $extras], $id_pago);
        }

        public function delete_extra($key, $id_pago)
        {
            $extras = $this->get_extras($id_pago);
            if ($extras && null !== $extras[$key]) {
                unset($extras[$key]);
                if (count($extras) > 0) {
                    return $this->save_extras($extras, $id_pago);
                } else {
                    return $this->update(['servicios_adicionales' => null], $id_pago);
                }
            }
        }

        public function reorganize_values($id_pago)
        {
            $pago = $this->get_payment($id_pago);
            $extras = $this->get_extras($pago['id_pago'], true);
    
            $updated_data = [
                  'total' => $pago['cuota'] + $extras['total'] + $pago['mora'],
                  'monto_extra' => $extras['total'],
                  'detalles_extra' => $extras['detalles']
            ];
    
            $this->update($updated_data, $pago['id_pago']);
        }
}