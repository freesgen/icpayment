<?php
namespace freesgen\application\models\eloquent;

class PaymentAgreement extends \Illuminate\Database\Eloquent\Model
{
    //
    protected $table = 'ic_acuerdos_pago';
    protected $guarded = ['created_at', 'updated_at'];
    protected $fillable =  ['id_contrato', 'id_cliente', 'fecha', 'cuotas', 'deuda_contrato','observaciones'];
    protected $primaryKey = "id_acuerdo_pago";

    public function empleado()
    {
        return $this->belongsTo('freesgen\application\models\eloquent\User', 'id_usuario', 'user_id');
    }

    public function contrato()
    {
        return $this->belongsTo('freesgen\application\models\eloquent\Contract', 'id_contrato', 'id_contrato');
    }

    public function contratview()
    {
        return $this->belongsTo('freesgen\application\models\eloquent\ContractView', 'id_contrato', 'id_contrato');
    }

    public function cliente()
    {
        return $this->belongsTo('freesgen\application\models\eloquent\Client', 'id_cliente', 'id_cliente');
    }

    public function payments()
    {
        return $this->hasMany('freesgen\application\models\eloquent\Payment', 'id_contrato', 'id_contrato')->where('servicios_adicionales', 'like', '%{%');
    }

}