<?php
/**
* IC Payment
*@author Jesus Guerrero
*@copyright Copyright (c) 2017 Insane Code
*@version 1.0.0
*
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Extra_model extends Crud {

  private $id_empleado;

  public function __construct(){
    parent::__construct();
    $this->tableName = 'ic_acuerdos_pago';
    $this->tableIdField = '';
    // You can initialize crud in here.
    // $this->initialize($config);
  }

	public function get_entries() {
		$query = $this->readData('*', $this->tableName);
		return $query->result_array();
	}

	public function insert_entry($data) {
		$this->createData($this->tableName, $data);
	}

	public function update_entry($id, $data) {
		$this->updateData($this->tableName, $data, ["$this->tableIdField" => $id]);
	}
}
