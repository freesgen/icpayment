<?php

class Comment_model extends CI_model
{

  function get_comments($id_averia){
    $this->db->where('id_averia', $id_averia);
    if($comments = $this->comment_view()){
      return $comments->result_array();
    }
    return false;
  }

  function get_comment($comment_id) {
    $this->db->where('id_reporte', $comment_id);
    if($comments = $this->comment_view(1)){
      return $comments->row_array();
    }
    return false;
  }

  function add_comment($comment) {
    if ($this->db->insert('ic_reporte_averias',$comment)) {
      return $this->get_comment($this->db->insert_id());
    }
    return false;
  }

  function delete_comment($id_comment) {
    $comment = $this->get_comment($id_comment);
    if ($this->db->delete('ic_reporte_averias',['id_reporte' => $id_comment])) {
      return $comment;
    }
    return false;
  }

  function update_comment($id_comment, $comment) {

  }

  function comment_view($limit = null) {
    return $this->db->select('ic_reporte_averias.* , concat(ic_users.name," ",ic_users.lastname) as `empleado`',false)
                ->join('ic_users','ic_reporte_averias.id_empleado = ic_users.user_id','LEFT')
                ->get('ic_reporte_averias', $limit);
  }
}
