<?php
class MY_Model extends CI_Model
{

	protected function relationships($modelResults, $tableName, $table, $alias, $idField)
	{
		foreach ($modelResults as $index => $model) {
			$result = $this->db
	    ->where($idField, $model[$idField])
	    ->get($tableName);
      $modelResults[$index][$alias] = $result->result_array();
		}
		return $modelResults;
	}
}
