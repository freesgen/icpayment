// if (isCurrentPage("servicios")) {
  (function () {
    var NewContract = new Vue({
      el: '#contract-form',
      data() {
        return {
          serviceId: '',
          priceId: '',
          firstPrice: 0,
          prices: [],
          mensualidad: '',
          instalacion: 0
        };
      },

      watch: {
        serviceId() {
          this.getPrices();
      }
      },

      mounted() {
        makeServiceCardClickable()
        bus.$on('service-clicked', service =>  {
          this.serviceId = service.serviceId;
          this.firstPrice = service.firstPrice;
          this.mensualidad = service.firstPrice;
          this.instalacion = service.firstPrice;
        })
      },

    methods: {
      setPrice(price) {
        this.mensualidad = this.prices.find(price => price.id_precio == this.priceId).monto;
        this.instalacion = this.mensualidad;
      },

      getPrices() {
        axios.get(`/prices/${this.serviceId}/service`)
          .then((res) => {
            const defaultPrice = {
              id_precio: 0,
              descripcion: 'por defecto',
              monto: this.firstPrice
            }

            this.prices = [defaultPrice]
            if (res.data && res.data.length) {
              this.prices.push(...res.data)
            }

            this.priceId = 0;
          });
      },

      activatePrice() {

      },
    }
  })
})();
// }
