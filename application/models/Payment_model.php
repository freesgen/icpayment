<?php
/**
* IC Payment
*@author Jesus Guerrero
*@copyright Copyright (c) 2017 Insane Code
*@version 1.0.0
*
*/
defined('BASEPATH') or exit('No direct script access allowed');

class Payment_model extends CI_MODEL
{
	public $id_pago = null;
	public $id_contrato;
	public $id_empleado = null;
	public $id_servicio = null;
	public $fecha_pago;
	public $concepto;
	public $detalles_extra = '';
	public $cuota;
	public $mora;
	public $monto_extra = 0;
	public $total;
	public $estado;
	public $fecha_limite;

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('lib_helper');
		$this->load->model('contract_model');
		$this->load->model('aditional_service_model');
	}

	/**
	*
	*@param array $data array with the data of the user
	*@param string $mode "normal" for save it in an insert, "full" to storage all the data
	*@return void
	*/
	public function organize_data($data, $mode)
	{
		if ($mode == 'full') {
			$this->id_pago = $data['id_pago'];
		}
		if (isset($data['id_empleado'])) {
			$this->id_empleado = $data['id_empleado'];
			$this->deuda = $data['deuda'];
			$this->abono_a = $data['abono_a'];
			$this->detalles_extra = $data['detalles_extra'];
		}
		$this->id_contrato = $data['id_contrato'];
		$this->id_servicio = $data['id_servicio'];
		$this->fecha_pago = $data['fecha_pago'];
		$this->concepto = $data['concepto'];
		$this->cuota = $data['cuota'];
		$this->mora = $data['mora'];
		$this->total = $data['total'];
		$this->estado = $data['estado'];
		$this->fecha_limite = $data['fecha_limite'];
	}

	public function add($data)
	{
		$this->organize_data($data, 'normal');
		if ($this->db->insert('ic_pagos', $this)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function get_payment($id, $as_receipt = false)
	{
		$table = ($as_receipt) ? 'v_recibos' : 'ic_pagos';
		$this->db->where('id_pago', $id);
		if ($result = $this->db->get($table)) {
			return $result->row_array();
		}
	}

	public function update($new_payment, $id_pago)
	{
		$where = ['id_pago' => $id_pago];
		if ($this->db->update('ic_pagos', $new_payment, $where)) {
			return true;
		}
		return false;
	}

	public function check_for_update($id_pago)
	{
		$sql = 'SELECT estado, generado from ic_pagos where id_pago =' . $id_pago;
		$result = $this->db->query($sql);
		$pago = $result->row_array();
		if ($pago['estado'] == 'no pagado') {
			$this->update('complete_date', 'now()', false);
			if (!$pago['generado']) {
				$this->check_extras_fijos($id_pago);
			}
			return true;
		} else {
			return false;
		}
	}

	public function get_next_payment_of($id_contrato)
	{
		$sql = "SELECT * FROM ic_pagos WHERE id_contrato = $id_contrato AND estado='no pagado' order by id_pago limit 1";
		$result = $this->db->query($sql);
		if ($result) {
			return $result->row_array();
		} else {
			return 0;
		}
	}

	// DEPRECATED: eliminar esto
	public function get_receipts($id)
	{
		$sql = "
			group_concat(id_pago) as id_pago,
			id_contrato,
			cliente,
			servicio,
			fecha_pago,
			empleado,
			fecha_limite,
			group_concat(DISTINCT concepto) as concepto,
			concat(group_concat(detalles_extra separator ' '),' | Meses: ', group_concat(monthname(fecha_limite) separator '*')) as detalles_extra,
			sum(mensualidad) as mensualidad,
			sum(mora) as mora,
			deuda,
			sum(monto_extra) as monto_extra,
			sum(total) as total,
			sum(descuento) as descuento,
			group_concat(DISTINCT razon_descuento) as razon_descuento";

		$this->db->select($sql, true);
		$this->db->where_in('id_pago', explode('_', $id));
		$this->db->group_by('cliente');

		if ($result = $this->db->get('v_recibos')) {
			return $result->row_array();
		} else {
			var_dump($this->db->last_query());
		}
	}

	public function get_extras($id_pago, $get_values = false)
	{
		$pago = $this->get_payment($id_pago);
		if ($pago['servicios_adicionales']) {
			$extras = json_decode($pago['servicios_adicionales'], 1);
			if (!$get_values) {
				return $extras;
			} else {
				$total_extras = 0;
				$string_detalles = '';
				$detalles_extras = [];

				foreach ($extras as $key => $extra) {
					$total_extras += $extra['precio'];
					$string_detalles .= "{$extra['servicio']} -- ";
					array_push($detalles_extras, $extra);
				}

				return ['total' => $total_extras, 'detalles' => $string_detalles, 'array' => $detalles_extras];
			}
		}
	}

	public function save_extras($extras, $id_pago)
	{
		$extras = json_encode($extras);
		return $this->update(['servicios_adicionales' => $extras], $id_pago);
	}

	public function set_extra($new_extra, $id_pago)
	{ // [ id_extra => ["servicio" => 'reconexion', "precio => 2000]]
		$extras = $this->get_extras($id_pago);
		if (!$extras) {
			$extras = $new_extra;
		} else {
			$key = join('', array_keys($new_extra));
			$extras[$key] = $new_extra[$key];
		}
		return $this->save_extras($extras, $id_pago);
	}

	public function delete_extra($key, $id_pago)
	{
		$extras = $this->get_extras($id_pago);
		if ($extras && isset($extras[$key])) {
			unset($extras[$key]);
			if (count($extras) > 0) {
				return $this->save_extras($extras, $id_pago);
			} else {
				return $this->update(['servicios_adicionales' => null], $id_pago);
			}
		}
	}

	public function check_extras_fijos($id_pago, $id_contrato = false)
	{
		if (!$id_contrato) {
			$id_contrato = $this->get_payment($id_pago)['id_contrato'];
		}

		$pago = $this->get_payment($id_pago);
		$contract = $this->contract_model->get_contract($id_contrato);
		$aditionalServices = $this->aditional_service_model->get_of_contract($id_contrato);
		if ($pago['abono_a'] == null && $pago['estado'] == 'no pagado') {
			if ($contract && $contract['extras_fijos']) {
				$this->set_extra([
					$contract['extras_fijos'] => [
						'servicio' => $contract['nombre_seguro'],
						'precio' => $contract['mensualidad_seguro']
					]
				], $id_pago);
			}

			if ($aditionalServices) {
				foreach ($aditionalServices as $aditional) {
					$aditionalService = [$aditional['id_servicio_adicional'] => [
						'servicio' => $aditional['nombre'],
						'precio' => $aditional['mensualidad']
					]];

					$this->set_extra($aditionalService, $id_pago);
				}
			}

			$this->reorganize_values($id_pago);
		}
	}

	public function reorganize_values($id_pago)
	{
		$pago = $this->get_payment($id_pago);
		$extras = $this->get_extras($pago['id_pago'], true);

		$updated_data = [
	  		'total' => $pago['cuota'] + $extras['total'] + $pago['mora'],
	  		'monto_extra' => $extras['total'],
	  		'detalles_extra' => $extras['detalles']
		];

		$this->update($updated_data, $pago['id_pago']);
	}

	// Contract related options

	public function count_unpaid_per_contract($id_contrato, $biggerThanToday = false)
	{
		try {
			$this->db->where('id_contrato', $id_contrato);
			$this->db->where('estado', 'no pagado');
			if ($biggerThanToday) {
				$this->db->where('fecha_limite > curdate()', null, true);
			}
			$result = $this->db->count_all_results('ic_pagos');
			if ($result) {
				return $result;
			} else {
				return 0;
			}
		} catch (Exception $e) {
			echo $this->db->last_query();
		}
	}

	public function count_of_contract($id_contrato = null)
	{
		if ($id_contrato == null) {
			$id_contrato = $_SESSION['last_payment_id'];
		}
		$this->db->where('id_contrato', $id_contrato);
		$result = $this->db->count_all_results('ic_pagos');
		if ($result) {
			return $result;
		} else {
			return 0;
		}
	}

	public function get_last_pay_of($id_contrato)
	{
		$sql = "SELECT * FROM ic_pagos WHERE id_contrato = $id_contrato and concepto not like '%abono%' order by id_pago desc limit 1";
		$result = $this->db->query($sql);
		if ($result) {
			return $result->row_array();
		} else {
			return 0;
		}
	}

	public function get_all_of_contract($id)
	{
		$this->db->where('id_contrato', $id);
		$this->db->order_by('-fecha_pago DESC,fecha_limite,complete_date', '', false);
		if ($result = $this->db->get('ic_pagos')) {
			echo make_payment_table($result->result_array(), 0);
		}
	}

	public function get_payments_of_contract($id)
	{
		$sql = "SELECT * FROM ic_pagos WHERE id_contrato = $id";
		$result = $this->db->query($sql);
		if ($result) {
			$result = $result->result_array();
			return $result;
		}
	}

	public function get_payments($id_contrato, $mode = false){
		if ($mode == 'list') {
		  $this->db->select("id_pago,id_contrato, monthname(fecha_limite) as mes, year(fecha_limite) as anio");
		} else if ($mode == 'table') {
		  $this->db->order_by('fecha_limite,-fecha_pago DESC, complete_date','',false);
		}
		$this->db->where('id_contrato',$id_contrato);
		if ($result = $this->db->get("ic_pagos")) {
		  if ($mode == 'table') {
			return make_payment_table($result->result_array(),0);
		  }
		  return $result->result_array();
		}
	  }

	public function getByAgreement($paymentAgreementId){
		$this->db->where('id_acuerdo_pago', $paymentAgreementId);
		if ($result = $this->db->get("ic_pagos")) {
			return make_payment_table($result->result_array(), 0);
		} else {
			echo $this->db->last_query();
		}
	  }

	public function list_all_of_contract($id_contrato)
	{
		$this->db->select('id_pago,id_contrato, monthname(fecha_limite) as mes, year(fecha_limite) as anio');
		$this->db->where('id_contrato', $id_contrato);
		if ($result = $this->db->get('ic_pagos')) {
			$result = make_payment_list($result->result_array());
			return $result;
		}
	}

	public function get_unpaid_per_contract($id_contrato)
	{
		$this->db->where('id_contrato', $id_contrato);
		$this->db->where('estado', 'no pagado');
		$result = $this->db->get('ic_pagos');
		if ($result) {
			return $result->result_array();
		}
	}

	public function update_payment_day_contract($id_contrato, $payment_day)
	{
		$sql = "
			UPDATE `ic_pagos` SET `fecha_limite` = concat(year(fecha_limite),'-',month(fecha_limite),'-$payment_day')
			WHERE `id_contrato` = ?
			and estado = 'no pagado'
			and fecha_limite > concat(year(curdate()),'-',month(curdate()),'-$payment_day')
		";
		if ($this->db->query($sql, [$id_contrato])) {
		}
	}

	// Grafic Related Options

	public function year_income()
	{
		$sql = 'SELECT sum(total) FROM v_recibos WHERE year(fecha_pago)=year(now())';
		$result = $this->db->query($sql);
		$result = $result->row_array()['sum(total)'];
		if ($result) {
			return $result;
		} else {
			return 0;
		}
	}

	public function month_income($mes)
	{
		$sql = "SELECT sum(total) FROM v_recibos WHERE year(fecha_pago)=year(now()) and month(fecha_pago)=$mes";
		$result = $this->db->query($sql);
		$result = $result->row_array()['sum(total)'];
		if ($result) {
			return $result;
		} else {
			return 0;
		}
	}

	public function get_incomes_per_month()
	{
		$resultado_por_mes = [];

		for ($i = 1; $i <= 12 ; $i++) {
			$value = $this->month_income($i);
			array_push($resultado_por_mes, $value);
		}
		return $resultado_por_mes;
	}

	public function day_income()
	{
		$now = date('Y-m-d');
		$sql = "SELECT sum(total) FROM v_recibos WHERE fecha_pago = '$now'";
		$result = $this->db->query($sql);
		$result->row_array()['sum(total)'];
		if ($result != null) {
			return $result->row_array()['sum(total)'];
		} else {
			return 0;
		}
	}

	public function weekday_income($day)
	{
		$sql = "SELECT sum(total) FROM v_recibos WHERE dayname(fecha_pago)='$day' and yearweek(fecha_pago) = yearweek(now())";
		$result = $this->db->query($sql);
		$result->row_array()['sum(total)'];
		if ($result != null) {
			return $result->row_array()['sum(total)'];
		} else {
			return 0;
		}
	}

	public function get_moras_view($mode = 'normal')
	{
    $isGroup = $mode == 'group';
    $sql = $this->moras_view_sql($isGroup);
		if ($isGroup) {
			$sql .= 'group by id_cliente;';
    }

      $result = $this->db->query($sql);
      return $result ?  $result->result_array() : [];
	}

	public function get_next_payments($expression = ['expression' => '1', 'unit' => 'MONTH'])
	{
		$sql = 'SELECT * FROM v_proximos_pagos WHERE fecha_limite BETWEEN now() and  adddate(now(), INTERVAL ' . $expression['expression'] . ' ' . $expression['unit'] . ')';
		if ($result = $this->db->query($sql)):
	  $result = $result->result_array();
		$result = make_next_payments_list($result);
		echo $result; else:
	  echo ' Error';
		endif;
	}

	public function get_moras_home()
	{
		$sql = 'SELECT * FROM v_pagos_pendientes';
		$result = $this->db->query($sql)->result_array();
		$result = make_next_payments_list($result);
		echo $result;
	}

	public function get_sum_monto_total_of($id_contrato)
	{
		$this->db->where('id_contrato', $id_contrato);
		$this->db->select_sum('cuota');
		$monto_total = $this->db->get('ic_pagos', 1)->row_array()['cuota'];
		return $monto_total;
	}

	public function moras_view_sql($isGroup = null) {
    $singleColumns = $isGroup ? "" : "
      `p`.`id_pago` AS `id_pago`,
      `p`.`id_contrato` AS `id_contrato`,
      `vc`.`cliente` AS `cliente`,
      `vc`.`codigo` AS `codigo`,
      `ips`.`ip_final` AS `ip_final`,
      `cli`.`celular` AS `celular`,
      `p`.`fecha_pago` AS `fecha_pago`,
      `p`.`concepto` AS `concepto`,
      `p`.`detalles_extra` AS `detalles_extra`,
      `p`.`cuota` AS `cuota`,
      `p`.`mora` AS `mora`,
      `p`.`monto_extra` AS `monto_extra`,
      `p`.`total` AS `total`,
      `p`.`estado` AS `estado`,
      `p`.`fecha_limite` AS `fecha_limite`,
      `p`.`complete_date` AS `complete_date`,
    ";

    $fecha_limite = $isGroup ? "ANY_VALUE(fecha_limite)" : "p.fecha_limite";
		return "
    select
      $singleColumns
			`vc`.`id_cliente` AS `id_cliente`,
			`cli`.`estado` AS `estado_cliente`,
			($fecha_limite + INTERVAL (`s`.`fecha_corte` + COALESCE(cli.dias_de_gracia, 0)) day) AS `fecha_corte`,
			COALESCE(cli.dias_de_gracia, 0) AS dias_de_gracia
			from ((((`ic_pagos` `p`
			join `ic_settings` `s` on((`s`.`id` = 1)))
			join `v_contratos` `vc` on((`vc`.`id_contrato` = `p`.`id_contrato`)))
			join `ic_clientes` `cli` on((`cli`.`id_cliente` = `vc`.`id_cliente`)))
			left join `v_ips` `ips` on((`ips`.`codigo` = `vc`.`codigo`)))
			 where ((cast(($fecha_limite + interval (`s`.`fecha_corte` + COALESCE(cli.dias_de_gracia, 0)) day) as date) < cast(now() as date)) and (`p`.`estado` = 'no pagado'))
		";
	}
}
