<?php

class Averias extends MY_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('averia_model');
    $this->load->model('comment_model');
    $this->load->model('contract_model');
  }

  public function add_averia() {
    authenticate();
    if ($data = $_POST) {
      if ($this->averia_model->add($data)) {
        $contract = $this->contract_model->get_contract_view($data['id_contrato']);
        $this->event->trigger('ticket', 1, $contract, " Descripcion: {$data['descripcion']}");
      }
    }

  }

  public function search() {
    authenticate();
    $data = json_decode($this->input->post('data'),true);
    $averias = $this->averia_model->search($data['text'] ,$data['state']);

    echo json_encode($averias);
  }

  public function get_averia() {
    authenticate();
    $data = json_decode($this->input->post('data'),true);
    $response['ticket']   = $this->averia_model->get_averia($data['id_averia']);
    $response['comments'] = $this->comment_model->get_comments($data['id_averia']);
    echo json_encode($response);
  }

  public function update_averia(){
    authenticate();
    $data = json_decode($this->input->post('data'),true);
    $id_averia = $data['id_averia'];
    unset($data['id_averia']);
    $response['mensaje'] = MESSAGE_ERROR. " No se pudieron guardar los cambios";

    if($this->averia_model->update_all($id_averia,$data)){
      $response['mensaje'] = MESSAGE_SUCCESS . "Cambios Guardados";

      $ticket = $this->averia_model->get_averia($id_averia);
      $this->event->trigger('ticket', 2, $ticket, " estado: {$ticket['estado']} | Descripcion: {$ticket['descripcion']} ");
    }

    echo json_encode($response);
  }

  public function delete_averia(){

  }

  // comments

  public function add_comment(){
    authenticate();
    $response['mensaje'] = MESSAGE_ERROR.' No se pudo agregar el reporte';
    $user    = get_user_data();
    $data = $this->get_post_data('data');
    $data = array_merge($data, ["id_empleado" => $user['user_id']]);

    if ($comment = $this->comment_model->add_comment($data)) {
      $response['mensaje'] = MESSAGE_SUCCESS.' reporte agregado';

      // event
      $ticket = $this->averia_model->get_averia($data['id_averia']);
      $ticket['comment'] = $comment;
      $this->event->trigger('comment', 1, $ticket, " Reporte: {$ticket['comment']['descripcion']} | estado: {$ticket['estado']} | Averia:
       {$ticket['descripcion']} ");

    }

    echo json_encode($response);
  }

  public function get_comments(){
    authenticate();
    $data = json_decode($this->input->post('data'),true);
    $response['comments'] = $this->comment_model->get_comments($data['id_averia']);
    echo json_encode($response);
  }

  public function delete_comment(){
    authenticate();
    $response['mensaje'] = MESSAGE_ERROR.' No se pudo borrar el reporte';
    $data = json_decode($this->input->post('data'),true);

    if ($comment = $this->comment_model->delete_comment($data['id_reporte'])) {
      $response['mensaje'] = MESSAGE_SUCCESS.' reporte Eliminado';

      //event
      $ticket = $this->averia_model->get_averia($comment['id_averia']);
      $ticket['comment'] = $comment;
      $this->event->trigger('comment', 4, $ticket, " Reporte: {$ticket['comment']['descripcion']} | estado: {$ticket['estado']} | Averia:
       {$ticket['descripcion']} ");
    }

    echo json_encode($response);
  }

  public function update_comment(){

  }

}
