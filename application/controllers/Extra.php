<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Extra extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("extra_model");
		$this->load->model("payment_model");
	}

	public function delete_extra(){
		authenticate();
    $data = $this->get_post_data('data');
    $is_admin =  auth_user_type(0);

		if ($data && isset($data['id']) && $is_admin) {
      $extra = $this->extra_model->get_extra($data['id'], true);
      if ($this->extra_model->delete_extra($data)) {
        // event
				$this->event->trigger('extra', 5, $extra);
      }
    } else if ($data) {
      $message = (!$is_admin) ? "debe ser administrador para eliminar este servicio": " No se ha podido eliminar el servicio extra";
      $res['mensaje'] =  MESSAGE_ERROR." $message";
      $this->response_json($res);
    }


	}

	public function get_extra_payment_of(){
		authenticate();
		if ($_POST){
			if(isset($_POST['data'])){
				$data = json_decode($_POST['data'],true);
			}else{
				$data = json_decode($_POST,true);
			}

				$res['pagos'] = $this->extra_model->get_all_of_extra($data['id_extra']);
				echo json_encode($res);
		}
	}

	public function get_all() {
		authenticate();
		$data = json_decode($_POST['data'],true);
		$res = $this->extra_model->get_all($data['state'], $data['text']);
		echo json_encode($res);
	}

	public function get_payment(){
		authenticate();
		$data = json_decode($_POST['data'],true);
		$res["recibo"] = $this->payment_model->get_payment($data["id_pago"]);
		echo json_encode($res);
	}

	public function apply_payment(){
		authenticate();
		$data = json_decode($_POST['data'],true);
		$info = json_decode($_POST['info'],true);
		if (!$this->payment_model->check_for_update($info['id_pago'])){
			$res['mensaje'] = MESSAGE_INFO.' Este pago ya ha sido realizado';
			echo json_encode($res);
		} else {
      if ($this->extra_model->apply_payment($data,$info)) {

        // event
        $receipt = $this->payment_model->get_payment($info['id_pago'], true);
        $this->event->trigger('payment', 3, $receipt, "pago de servicio extra");
      }
		}
	}

	public function edit_payment() {
		authenticate();
		$data = json_decode($_POST['data'],true);
		$info = json_decode($_POST['info'],true);
		if (!$this->payment_model->check_for_update($info['id_pago']) && $data){
			 if ($this->extra_model->apply_payment($data,$info)) {
         // event
        $receipt = $this->payment_model->get_payment($info['id_pago'], true);
        $this->event->trigger('payment', 2, $receipt, "pago de servicio extra");
       }
		}
	}

	public function delete_payment(){
    authenticate();
    $data = $this->get_post_data('data');
    $is_admin =  auth_user_type(0);

		if ($data && isset($data['id_pago']) && $is_admin) {
      $receipt = $this->payment_model->get_payment($data['id_pago'], true);
      if ($this->extra_model->delete_payment($data)) {
        // event
        $this->pusher->trigger('my-channel','global', "ya tu sabe");
        $this->event->trigger('payment', 4, $receipt, "pago de servicio extra");
      }
    } else if ($data) {
      $message = (!$is_admin) ? "debe ser administrador para eliminar este pago": " No se ha podido registrar el pago";
      $res['mensaje'] =  MESSAGE_ERROR." $message";
      $this->response_json($res);
    }
	}

	public function generate_extra_payment(){
		authenticate();
		$data = json_decode($_POST['data'],true);
		$this->extra_model->generate_extra_payment($data);
	}

	public function has_extra() {
		authenticate();
		$data = json_decode($_POST['data'],true);
		$res  = $this->extra_model->has_extra($data['id_cliente']);
		echo json_encode($res);
	}

}
