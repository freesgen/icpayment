<?php
namespace freesgen\application\models\eloquent;

class Service extends \Illuminate\Database\Eloquent\Model
{
    //
    protected $table = 'ic_servicios';
    protected $guarded = ['created_at', 'updated_at'];

    public function precios()
    {
        return $this->hasMany('freesgen\application\models\eloquent\Price', 'id_servicio', 'id_servicio');
    }
}