<?php

defined('BASEPATH') or exit('No direct script access allowed');
use freesgen\application\controllers\My_Crud;
use freesgen\application\models\eloquent\PaymentAgreement;
use freesgen\application\models\eloquent\Payment;
use freesgen\application\models\eloquent\Contract;
use freesgen\application\traits\ReporterTrait;
use freesgen\application\listeners\PaymentAgreement as PaymentAgreementListener;
class PaymentAgreements extends My_Crud
{
	 	use ReporterTrait;
		protected $modelName = 'paymentAgreement';
		protected $reportEndpoint = 'app/imprimir/acuerdo_pago';
    protected $listener = PaymentAgreementListener::class;
    protected $model = PaymentAgreement::class;
		protected $fields = ['fecha', 'codigo'];
		
		public function __construct() {
			parent::__construct();
			$this->load->model('contract_model');
			$this->load->model('settings_model');
			$this->load->model('service_model');
			$this->load->model('payment_model');
			$this->load->model('client_model');

		}
	
    public function create() {
			$data = $this->get_post_data('data');
			if ($data) {
				$contrato = Contract::find($data['id_contrato']);
				$user = $this->my_auth->get_user_data();
				$data['id_usuario'] = $user['user_id'];

				if ($contrato && $contrato->estado == 'suspendido') {
					$this->db->where("id_contrato", $data['id_contrato'])->where('fecha_limite', $data['fecha']);
					$number = $this->db->count_all_results('ic_pagos');
					$data['id_servicio'] = $contrato->id_servicio;
					$data['id_precio'] = $contrato->id_precio;
					$data['duracion'] = 12;
	
					if ($number == 0) {
							$this->db->trans_start();

							// cancelamos el contrato primero para quitar los pagos
							cancel_contract($this, 
							[
								"id_contrato" => $data['id_contrato'],
								"motivo" => "acuerdo de pago",
								"penalidad" => false,
								"fecha" => date("Y-m-d")
							], false, false);

							// Y reconectamos el contrato
							reconnect_contract($data, $this);
							$this->contract_model->delete_cancelation($data['id_contrato']);
							$this->createCharges($data);
							$this->db->trans_complete();
	
							if ($this->db->trans_status() === false) {
									$message = " El contrato/cliente no pudo ser reconectado";
									$messageType = "error";
								} else {
									$this->model::create($data);
									$message = "Acuerdo de pago generado";
									$messageType = "success";
							}

							$returnData = [
									"message" => [
										'type' => $messageType,
										'text' => $message
									],
									"data" => []
							];

					} else {
						$returnData = [
							"message" => [
								"type" => "error",
								"text" => "Ya hay pagos para esta fecha"
							],
							"data" => []
						];
					}

				} else {
					$returnData = [
						"message" => [
							"type" => "error",
							"text" => "El contrato debe estar en estado suspendido"
						],
						"data" => []
					];
				}

			}

			return $this->response_json($returnData);	
	  }

	public function edit($id) {
		$data = json_decode($this->input->input_stream('data'), 1);
		if ($data) {
			$user = $this->my_auth->get_user_data();
			$data['id_usuario'] = $user['user_id'];

			try {
				$resource = $this->model::find($id);
				$resource->fill($data);

				if ($resource->save()) {
					$this->createCharges($data);

					$returnData = [
							"message" => [
								'type' => 'success',
								'text' => "El acuerdo de pago ha sido modificado"
							],
							"data" => $resource
					];
					
					if ($this->listener) {
						$this->emitter->emit("$this->modelName::*", $resource);
					}
				}
			} catch(\Exception $e) {
				echo $e->getMessage();
			}

			} else {
				$returnData = [
					"message" => [
						"type" => "error",
						"text" => "El acuerdo de pago no pudo ser guardado"
					],
					"data" => []
				];
			}

		return $this->response_json($returnData);	
	}

	public function destroy($paymentAgreementId) {
		$paymentAgreement = PaymentAgreement::find($paymentAgreementId);
		if ($paymentAgreement && $paymentAgreement->delete($paymentAgreement)) {
			$this->destroyPayments($paymentAgreementId);	
			$this->set_message('Acuerdo de pago eliminado con exito');
		} else {
			$this->set_message('Este acuerdo de pago no existe', 'error');
		}

		$this->response_json();

	}

	private function createCharges($data) {
		$contract_date = new DateTime($data['fecha']);
		$next_payment_date = $contract_date;
		$fecha = $next_payment_date->format('Y-m-d');
		$pagos = $this->db->select('id_pago')->where("cast(fecha_limite as date) >= cast('$fecha' as date)")->where('id_contrato', $data['id_contrato'])->get('ic_pagos')->result_array();
		$iterations = 0;

		foreach ($data['montoCuotas'] as $cuota) {
			for ($i=0; $i < $cuota['cantidad']; $i++) { 
				if ($cuota['cantidad'] > 0 && $cuota['amount'] > 0)
					$cargo = $cuota['amount'];
					$nombreCargo = "Cuota de acuerdo de pago " . ($iterations + 1);
					$idPago = $pagos[$iterations]['id_pago'];
					$this->payment_model->set_extra(['cargo::' => ["servicio" => $nombreCargo, "precio"=> $cargo]], $idPago);
					$this->payment_model->reorganize_values($idPago);
					$iterations++;
			}
		}	
		
		if (isset($data['firstPayment']) and $data['firstPayment']) {
			$this->createFirstPayment($data);
		}
	}

	private function updatePayments($paymentAgreementId, $data) {
		$contract_date = new DateTime($data['fecha']);
		$next_payment_date = $contract_date;
		$day = $contract_date->format('d');

		$paymentCounter = 0;
		foreach ($data['montoCuotas'] as $cuota) {
			$cantidad = isset($cuota['cantidad']) ?  $cuota['cantidad']: 0;
			for ($i=0; $i < $cantidad; $i++) {

				$paymentCounter++;
				if (isset($cuota['cantidad']) && $cuota['amount'] > 0)
					if (isset($cuota['id_pago']) && $payment = Payment::find($cuota['id_pago'])) {

						$cuota['cuota'] = $cuota['amount'];
						$cuota['total'] = $cuota['amount'];
						$cuota['fecha_limite'] = $next_payment_date->format('Y-m-d');
						$cuota['concepto'] = "Pago de acuerdo " . ($paymentCounter);
						$cuota = array_except($cuota, ['cantidad', 'amount']);
						$payment->fill($cuota);
						$payment->save();

					} else {
						$payment = Payment::create([
							'id_acuerdo_pago'   => $paymentAgreementId,
							'id_servicio' => null,
							'id_precio' => null,
							'id_empleado' => $data['id_usuario'],
							'fecha_pago' => null,
							'concepto' => "Pago de acuerdo " . ($paymentCounter),
							'cuota' => $cuota['amount'],
							'mora' => 0,
							'total' => $cuota['amount'],
							'estado' => 'no pagado',
							'fecha_limite' => $next_payment_date->format('Y-m-d')
						]);

						$this->model->payments($payment);
					}
					$next_payment_date = get_next_date($next_payment_date, $day);
			}
		}	
		
		$this->destroyPaymentsByIds($data['eliminados']);
	}

	public function createFirstPayment($data) {
		$new_data = [
			'id_contrato' => $data['id_contrato'],
			'id_servicio' => null,
			'id_precio' => null,
			'fecha_pago' => null,
			'concepto' => '50% de acuerdo de pago',
			'cuota' => $data['firstPayment'],
			'mora' => 0,
			'total' =>  $data['firstPayment'],
			'estado' => 'no pagado',
			'fecha_limite' => $data['fecha']
			];
			
		$this->payment_model->add($new_data);
	}
	
	private function destroyPayments($paymentAgreementId) {
		Payment::where('id_acuerdo_pago', $paymentAgreementId)
		->delete();
	}
	private function destroyPaymentsByIds($paymentIds) {
			if (count($paymentIds)) {
				Payment::whereIn('id_pago', $paymentIds)
				->delete();
			}
	}
}