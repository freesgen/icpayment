
const path        = './src/'
const Path        = require('path');
// my files

var Head = [
  path + "js/lib/jquery-3.2.1.min.js",
  path + "js/lib/bootstrap.min.js",
  path + "js/lib/moment.min.js",
  path + "js/lib/locale/es-do.js",
  path + "js/lib/Chart.js",
  path + "js/lib/currencyFormat.js"
]

const Libraries = [
  path + "js/lib/vue.min.js",
  path + "js/lib/axios.min.js",
	path + "bt/bootstrap-table.min.js",
  path + "bt/locale/bootstrap-table-es-SP.min.js",
  path + "js/lib/sweetalert2.min.js",
  path + "js/lib/jquery.inputmask.js",
	path + "js/lib/icheck.min.js",
  path + "js/lib/peace.min.js",
  path + "js/lib/select2.full.min.js"
]

const Common = [
  path + "js/tables/clientTable.js",
  path + "js/tables/serviceTable.js",
  path + "js/tables/contractTable.js",
  path + "js/tables/adminTables.js",
  path + "js/tables/paymentTable.js",
  path + "js/tables/sectionTable.js",
  path + "js/lib/globals.js",
  path + "js/base.js",
  path + "js/controllers.js",
  path + "js/ajax.js",
  path + "js/ajax2.js",
]

const Components  = `${path}js/components/**.js`

// sass files

const cssFiles    = [
  path + "scss/5-others/material-icons.css",
  path + "scss/5-others/font-awesome.min.css",
  path + "scss/5-others/bootstrap.min.css",
  path + "scss/5-others/sweetalert2.min.css",
  path + "bt/bootstrap-table.min.css"
]

const frontendCss = [
  path + "scss/5-others/peace-material.min.css",
  path + "scss/5-others/select2.min.css",
  path + "scss/5-others/AdminLTE.min.css",
  path + "scss/5-others/square/blue.css",
  path + "scss/5-others/square/square.css",
]

const superPath   = './'
const distTest    = Path.resolve(superPath,'src','js','test')
const dist        = Path.resolve(superPath,'assets','js','dist')
const distMin     = Path.resolve(superPath,'assets','js','min')

// sass taks

gulp.task('sass', () => {
  const mainMinCSS = gulp.src([`${path}/css/**/*.sass`,`!${path}assets/css/_*.sass`, `${path }assets/css/*.sass}`])
    .pipe(sass({outputStyle:'compressed'}).on('error', sass.logError))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(path + '../assets/css'))

  const secundaryMinCSS = gulp.src(cssFiles)
    .pipe(concat("secundaryCss.min.css"))
    .pipe(cleanCss())
    .pipe(gulp.dest(path + '../assets/css'))

    const frontendMinCSS = gulp.src(frontendCss)
    .pipe(concat("frontend.min.css"))
    .pipe(cleanCss())
    .pipe(gulp.dest(path + '../assets/css/5-others/square'))

  return merge(mainMinCSS, secundaryMinCSS, frontendMinCSS)
});


gulp.task('watch', () => {
  gulp.watch(`${path }css/**`,['sass']);
  gulp.watch([`${path}js/**/*.js`,`!${path}js/test/**`], ['final-compress']);
});

gulp.task('default',['watch',"sass","final-compress"]);
