alter VIEW `v_morosos`
AS select `p`.`id_pago` AS `id_pago`,
`p`.`id_contrato` AS `id_contrato`,
`vc`.`id_cliente` AS `id_cliente`,
`vc`.`cliente` AS `cliente`,
`vc`.`codigo` AS `codigo`,
`ips`.`ip_final` AS `ip_final`,
`cli`.`celular` AS `celular`,
`p`.`fecha_pago` AS `fecha_pago`,
`p`.`concepto` AS `concepto`,
`p`.`detalles_extra` AS `detalles_extra`,
`p`.`cuota` AS `cuota`,
`p`.`mora` AS `mora`,
`p`.`monto_extra` AS `monto_extra`,
`p`.`total` AS `total`,
`p`.`estado` AS `estado`,
`p`.`fecha_limite` AS `fecha_limite`,
`p`.`complete_date` AS `complete_date`,
`cli`.`estado` AS `estado_cliente`,
ADDDATE(`p`.`fecha_limite`, INTERVAL s.fecha_corte DAY) AS `fecha_corte`
from ((((`ic_pagos` `p` join `ic_settings` `s` on((`s`.`id` = 1))) join `v_contratos` `vc` on((`vc`.`id_contrato` = `p`.`id_contrato`)))
join `ic_clientes` `cli` on((`cli`.`id_cliente` = `vc`.`id_cliente`)))
left join `v_ips` `ips` on((`ips`.`codigo` = `vc`.`codigo`)))
where (cast(ADDDATE(`p`.`fecha_limite`, INTERVAL s.fecha_corte DAY) as date) < cast(now() as date))
and (`p`.`estado` = 'no pagado')

--  new change
CREATE TABLE `ic_precios` (
  `id_precio` int(11) NOT NULL AUTO_INCREMENT,
  `id_servicio` int(11) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `monto` decimal(11,2) DEFAULT NULL,
  `activo` bool DEFAULT NULL,
  PRIMARY KEY (`id_precio`),
  KEY `fk_p_id_servicio` (`id_servicio`),
  CONSTRAINT `fk_p_id_servicio` FOREIGN KEY (`id_servicio`) REFERENCES `ic_servicios` (`id_servicio`)
)

alter table ic_contratos add id_precio int(11);
alter table ic_pagos add id_precio int(11);

ALTER VIEW `v_contratos`
AS
select `c`.`id_contrato` AS `id_contrato`,
`c`.`codigo` AS `codigo`,
`cli`.`id_cliente` AS `id_cliente`,
`cli`.`cedula` AS `cedula`,concat(`cli`.`nombres`,' ',`cli`.`apellidos`) AS `cliente`,
concat(`cli`.`calle`,' #',`cli`.`casa`,', ',`cli`.`sector`,', ',`cli`.`provincia`) AS `direccion`,
`c`.`fecha` AS `fecha`,
`c`.`duracion` AS `duracion`,
`c`.`id_servicio` AS `id_servicio`,
`c`.`id_precio` AS `id_precio`,
concat(`s`.`nombre`, ' ', `pre`.`descripcion`) AS `servicio`,
`s`.`descripcion` AS `descripcion`,
ifnull(`pre`.`monto`, `s`.`mensualidad`) AS `cuota`,
`c`.`monto_pagado` AS `monto_pagado`,
`c`.`monto_total` AS `monto_total`,
`c`.`ultimo_pago` AS `ultimo_pago`,
`c`.`proximo_pago` AS `proximo_pago`,
`c`.`estado` AS `estado`,
`c`.`nombre_equipo` AS `nombre_equipo`,
`c`.`mac_equipo` AS `mac_equipo`,
`c`.`router` AS `router`,
`c`.`mac_router` AS `mac_router`,
`c`.`modelo` AS `modelo`,
`c`.`ip` AS `ip`,
`c`.`extras_fijos` AS `extras_fijos`
from (((`ic_contratos` `c`
join `ic_clientes` `cli`)
join `ic_pagos` `p`)
join `ic_servicios` `s`)
left join `ic_precios` `pre` on((`c`.`id_precio` = `pre.id_precio`))
where ((`c`.`id_cliente` = `cli`.`id_cliente`) and (`c`.`id_contrato` = `p`.`id_contrato`) and (`c`.`id_servicio` = `s`.`id_servicio`)) group by `c`.`id_contrato`


v_recibos VIEW `v_recibos`
AS
select `p`.`fecha_pago` AS `fecha_pago`,
`p`.`id_pago` AS `id_pago`,
`p`.`id_contrato` AS `id_contrato`,
`p`.`id_extra` AS `id_extra`,
ifnull(concat(`cli`.`nombres`,' ',`cli`.`apellidos`),concat(`cli2`.`nombres`,' ',`cli2`.`apellidos`)) AS `cliente`,
`p`.`concepto` AS `concepto`,
`p`.`detalles_extra` AS `detalles_extra`,
`s`.`nombre` AS `servicio`,
`p`.`cuota` AS `mensualidad`,
`p`.`mora` AS `mora`,
`p`.`monto_extra` AS `monto_extra`,
`p`.`total` AS `total`,
`p`.`estado` AS `estado`,
concat(`u`.`name`,' ',`u`.`lastname`) AS `empleado`,
`p`.`complete_date` AS `complete_date`,
`p`.`fecha_limite` AS `fecha_limite`,
`p`.`descuento` AS `descuento`,
`p`.`razon_descuento` AS `razon_descuento`,
`p`.`deuda` AS `deuda`,
`p`.`abono_a` AS `abono_a`,
`p`.`tipo` AS `tipo`
from ((((((`ic_pagos` `p` left join `ic_contratos` `c` on((`c`.`id_contrato` = `p`.`id_contrato`)))
left join `ic_clientes` `cli` on((`cli`.`id_cliente` = `c`.`id_cliente`)))
left join `ic_servicios` `s` on((`p`.`id_servicio` = `s`.`id_servicio`)))
left join `ic_servicios_extra` `ex` on((`p`.`id_extra` = `ex`.`id_extra`)))
left join `ic_clientes` `cli2` on((`ex`.`id_cliente` = `cli2`.`id_cliente`)))
left join `ic_users` `u` on((`p`.`id_empleado` = `u`.`user_id`)))
where (`p`.`estado` = 'pagado')
