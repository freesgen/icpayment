<div class="screen clients row">
  <?php
	$client_data = get_client_data();
	$nombre_completo = 'Acuerdo De Pago';
	$iniciales = 'AP';
	$active_window = 'pagos';
	$active = '';

	if (isset($_SESSION['active_window'])) {
		$active_window = $_SESSION['active_window'];
	}
  ?>
  <div class="main-content detalles col-md-12">

    <div class="row">
      <div class="col-xs-6 col-md-3 center-row aside-wide-left">
        <div class="page-header">
          <h3>Cuotas de Acuerdo de pago</h3>
        </div>

        <div class="client-profile">
          <span><?php echo $iniciales ?></span>
        </div>
        <h5>
          <?php echo $nombre_completo ?>
        </h5>
        <?php
          if ($active_window == 'pagos'):
          $controls_class = 'visible';
          else:
          $controls_class = '';
          endif;
		    ?>
        <div class="payment-controls <?php echo $controls_class ?>" id="payment-controls">
          <div class="input-group">
            <span class="input-group-addon" id="addon">Contrato </span>
            <input class="form-control" id="select-contract"  disabled value="<?php echo $params ?>" />
          </select>
          </div>
          <div class="box" id="payment-detail-box">
            <div class="box-header with-border">
              <h3 class="box-title">Detalles de Pago</h3>
              <div class="box-tools pull-right">
                <!-- Collapse Button -->
                <button type="button" class="mybtn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <h6>Forma de pago</h6>
            <div class="btn-group" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-small payment-mode">efectivo</button>
              <button type="button" class="btn btn-small payment-mode">banco</button>
            </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <button class="btn" id="btn-pay" v-show="!print_mode">Registrar Pago</button>
          <button class="btn" v-cloak :class="btnClass" id="btn-multiple-receipt" @click="toggleMode">{{ modeText }}</button>
          <a v-cloak v-show="showReceipt" :href="linkReceipt" target="_blank"> Imprimir Recibo </a>
        </div>
        <div class="contract-controls hide">
          <button class="btn icon" id="btn-detail-cancel-contract" disabled><i class="material-icons" title="cancelar contrato" >delete</i></button>
          <button class="btn icon" id="btn-detail-suspend-contract" title="suspender contrato" disabled><i class="material-icons" >report_problem</i></button>
          <button class="btn icon" id="btn-call-reconnect" title="Reconectar" disabled><i class="material-icons" >fiber_smart_record</i></button>
          <button class="btn icon" id="btn-call-extra" title="extras" disabled><i class="material-icons" >more</i></button>
        </div>
      </div>
      <div class="col-md-9 wide-main-content">
        <div>

          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist" id="main-tabs">
  
            <li role="presentation" <?php if ($active_window == 'pagos'):?>class="active"
              <?php endif; ?>><a href="#payments" aria-controls="messages" role="tab" data-toggle="tab">Pagos</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <!---->
            <div role="tabpanel" class="tab-pane detail-panel fade in <?php if ($active_window == 'pagos'):?> active <?php endif; ?>" id="payments">
              <div>
                <div id="payment-vue">
                  <contract-detail-section></contract-detail-section>
                </div>
                <table 
                  class="table t-pagos innertable" id="t-pagos"
                  data-minimum-count-columns="2"
                  data-show-pagination-switch="false"
                  data-pagination="false"
                  data-id-field="id"
                  data-page-size="-1"
                  data-page-list="[10,20,50,-1]"
                  data-show-footer="false"
                  data-striped="false"
                  data-click-to-select="true"
                  data-single-select="true">
                  <thead>
                    <tr>
                      <th data-field="deshacer"></th>
                      <th data-field="id" class="hide">ID Pago</th>
                      <th data-field="checkbox" data-checkbox="true" class="hide"> </th>
                      <th data-field="concepto">Concepto</th>
                      <th data-field="cuota">Cuota</th>
                      <th data-field="mora">Mora</th>
                      <th data-field="extra">Extra</th>
                      <th data-field="monto">Monto</th>
                      <th data-field="fecha_pago">Fecha de Pago</th>
                      <th data-field="estado" data-align="center">Estado</th>
                      <th data-field="fecha_limite">Vence En</th>
                      <th data-field="action">Recibo</th>
                      <th data-field="id_contrato" class='hide'>ID Contrato</th>
                      <th data-field="control">Control</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>


          </div>
        </div>
      </div>
    </div>

  </div>
</div>
