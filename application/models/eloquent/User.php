<?php
namespace freesgen\application\models\eloquent;

class User extends \Illuminate\Database\Eloquent\Model
{
    //
    protected $table = 'ic_users';
    protected $guarded = ['created_at', 'updated_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];
}