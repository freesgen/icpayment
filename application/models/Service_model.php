<?php
/**
* IC Payment
*@author Jesus Guerrero
*@copyright Copyright (c) 2017 Insane Code
*@version 1.0.0
*
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_model extends CI_MODEL{

  public $id_servicio = null;
  public $nombre;
  public $descripcion;
  public $mensualidad;
  public $tipo;


  public function __construct(){
    parent::__construct();
    $this->load->helper('lib_helper');
  }

  /**
  *
  *@param array $data array with the data of the user
  *@param string $mode "normal" for save it in an insert, "full" to storage all the data
  *@return void
  */

  function organize_data($data,$mode){

    if($mode == "full"){
      $this->id_servicio   = $data['id_servicio'];
    }
    $this->nombre          = $data['nombre'];
    $this->descripcion     = $data['descripcion'];
    $this->mensualidad     = $data['mensualidad'];
    $this->tipo            = $data['tipo'];
  }

  public function add($data){
    $this->organize_data($data,"normal");
    $result = $this->db->query("SELECT * FROM ic_servicios WHERE nombre = '". $this->nombre . "'");
    $result = $result->result_array();
    $result = count($result);
    if($result){
      echo MESSAGE_ERROR." Este nombre ya está registrado";
    }else{

      if($this->db->insert('ic_servicios',$this)){
				echo MESSAGE_SUCCESS." Servicio Agregado con exito";
				return true;
      }else{
       echo "No pudo guardarse el servicio";
      }
    }

  }

  public function update_service($data){
		$service = $this->get_service($data['id_servicio']);

    $data_for_update = [
      'nombre'      => $data['nombre'],
      'descripcion' => $data['descripcion'],
      'mensualidad' => $data['mensualidad'],
      'tipo'        => $data['tipo']
		];

    $this->db->where('id_servicio', $data['id_servicio']);
    if ($this->db->update('ic_servicios',$data_for_update)){
			echo MESSAGE_SUCCESS." Servicio Actualizado Con Exito!";

			if ($data['mensualidad'] == $service['mensualidad']) {
				return true;
      } else {
        return update_contract_from_service($data);
      }
    } else {

      echo MESSAGE_ERROR." No pudo actualizarse el servicio ";
    }
  }

  public function get_all_services(){
    $this->db->order_by('tipo, mensualidad');
    $result = $this->db->get('ic_servicios');
    echo make_service_table($result->result_array(),0);
  }

  public function search_services($word){
    $fields = array(
     'id_servicio'  => $word,
     'nombre'       => $word,
     'descripcion'  => $word
    );
    $this->db->or_like($fields);
    $this->db->order_by('tipo, mensualidad');
    if($result = $this->db->get('ic_servicios')){
      echo  make_service_table($result->result_array(),0);
    }
  }

  public function get_services_shortcuts(){
    $sql    = "SELECT * FROM ic_servicios WHERE tipo= 'internet' order by mensualidad";
    $result = $this->db->query($sql);
    echo make_service_shortcuts($result->result_array());
  }

  public function get_services_dropdown(){
    $sql = "SELECT * FROM ic_servicios WHERE tipo= 'reparacion'";
    $result = $this->db->query($sql);
    echo make_other_services_dropdown($result->result_array());
  }

  public function count_services(){
    $result = $this->db->count_all("ic_servicios");
    echo $result;
  }

  public function get_service($id){
    $sql = "SELECT * FROM ic_servicios WHERE id_servicio='$id' or nombre='$id' limit 1";
    $result = $this->db->query($sql);
    if($result){
      $result =$result->row_array();
      return $result;
    }
  }

  public function delete_service($id){
    $sql = "DELETE FROM ic_servicios WHERE id_servicio= $id";
    if($this->db->query($sql)){
			echo MESSAGE_SUCCESS." Servicio Eliminado";
			return true;
    }else{
      echo MESSAGE_ERROR." Error: Puede que este servicio tenga contratos vinculados";
    }
  }

  //functions
}
