<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Prices extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('price_model');
		$this->load->model('service_model');
		$this->load->model('contract_view_model');
		$this->load->model('payment_model');
	}

	public function get($id, $mode = 'service')
	{
		authenticate();
		if ($mode === 'service') {
			$res = $this->price_model->get_of_service($id);
		} else {
			$res = $this->price_model->get($id);
		}
		$this->response_json($res);
	}

	public function create()
	{
		authenticate();
		$data = $this->get_post_data('data');
		$service = $this->service_model->get_service($data['id_servicio']);
		try {
			if ($service['tipo'] !== 'internet') {
				throw new Exception('Precios solo disponibles para servicios de internet', 2);
			} else {
				$res['service'] = $this->price_model->add($data);
				$this->response_json($res);
			}
		} catch (Exception $e) {
			$this->set_error_response(['message' => $e->getMessage()]);
		}
	}

	public function delete($priceId)
	{
		authenticate();
		if ($priceId) {
			$result = $this->price_model->delete($priceId);
			if ($result) {
				$this->response_json(['status' => 200, 'message' => 'Precio eliminado']);
			} else {
				$this->set_error_response(['status' => 400, 'message' => 'el precio no pudo ser eliminado']);
			}
		}
	}

	public function update($priceId)
	{
		authenticate();
    $data = $this->get_post_data('data');
		if ($priceId) {
				try {
          $res = $this->price_model->update($priceId, $data);
          $this->response_json($res);
        } catch (Exception $e) {
          $this->set_error_response(['message' => $e->getMessage()]);
        }
		}
	}
}
