<?php
/**
* IC Payment
*@author Jesus Guerrero
*@copyright Copyright (c) 2017 Insane Code
*@version 1.0.0
*
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Averia_model extends CI_MODEL{

  #$id_averia = null;
  #id_cliente
  #id_contrato
  #descripcion
  #fecha
  #estado

  public function __construct(){
    parent::__construct();

  }

  public function add($data){
    $rows = array(
      'id_averia'   => null,
      'id_cliente'  => $data['id_cliente'],
      'id_contrato' => $data['id_contrato'],
      'descripcion' => $data['descripcion'],
      'fecha'       => null,
      'estado'      => 'por reparar',
    );
    if($this->db->insert('ic_averias',$rows)){
      echo MESSAGE_SUCCESS."Averia Guardada";
      return true;
    }else{
      echo MESSAGE_ERROR."error". " Error";
    }
  }

  public function update($id_averia){
    $this->db->where('id_averia',$id_averia);
    $result = $this->db->get('v_averias',1);
    $status = $result->row_array()['estado'];
    switch ($status) {
      case 'por reparar':
        $status = 'reparado';
        $fecha_reparacion = date('Y-m-d');
        break;
      default:
        $status =  'por reparar';
        $fecha_reparacion = '';
    }
    $this->db->where('id_averia',$id_averia);
    if($this->db->update('ic_averias',array("estado" => $status,"fecha_reparacion" => $fecha_reparacion))){
      echo MESSAGE_SUCCESS." Estado de averia cambiado a ". $status;
      return true;
    }
  }

  public function update_all($id_averia,$data) {
    $this->db->where('id_averia',$id_averia);
    return (bool) $this->db->update('ic_averias',$data);
  }

  public function get($status = 'por reparar', $type = 'html'){
    if($status != 'todos'){
      $this->db->where('estado',$status);
    }
    $this->db->order_by('fecha','DESC');
    $result = $this->db->get('v_averias');
    $result = $result->result_array();
    $resultCount = $result ? count($result) : 0;

    $_SESSION['averias'] = $result;
    
    if ($result and $resultCount > 0) {

      if ($type == 'html' ) {
        echo make_averias_list($result);
      } else {
        return $result;
      }
    }else{
      echo "<h3>No hay Datos Para Esta Busqueda</h3>";
    }

  }

  public function count(){
    $result = $_SESSION['averias'];
    if ($result){
      $result = count($result);
      echo $result;
    }else{
      echo 0;
    }
  }

  public function search($word, $status) {

    if (substr($word, 0, 1) === '#') {
      $id_averia = substr($word, 1);
      $this->db->where('id_averia', $id_averia);
      $word = '';
    }
    else if ($status != "todo") {
      $this->db->where('estado', $status);
    }

    $this->db->like('cliente', $word);
    $this->db->order_by('fecha','DESC');
    if ($result = $this->db->get('v_averias')) {
      return make_averias_list($result->result_array());
    }
    return false;
  }

  public function get_averia($id_averia){
    $this->db->select('v_averias.* , v_contratos.codigo',false);
    $this->db->where('id_averia',$id_averia);
    $this->db->join('v_contratos','id_cliente','LEFT');

    if ($averia = $this->db->get('v_averias',1)) {
      return $averia->row_array();
    }
    return false;
  }

  public function restructure() {
    $tickets = $this->db->get('ic_averias');
    $tickets = $tickets->result_array();
    $newStructure = [];
    foreach($tickets as $ticket) {
      $id = $this->getContractId($ticket['id_cliente']);
      $ticket['id_contrato'] = $id;
      $this->db->update('ic_averias', ['id_contrato' => $id], ['id_cliente' => $ticket['id_cliente']]);
      array_push($newStructure, $ticket);
    }
    return $newStructure;
  }

  private function getContractId($client_id) {
    $contract = $this->db->where('id_cliente', $client_id)
                ->select('id_contrato')
                ->get('ic_contratos', '1');
    if($contractId = $contract->row_array()['id_contrato']){
      return $contractId;
    }else {
      var_dump($this->db->last_query());
    }
  }
}
