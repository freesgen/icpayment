<?php
namespace freesgen\application\models\eloquent;

class Contract extends \Illuminate\Database\Eloquent\Model
{
    //
    protected $table = 'ic_contratos';
    protected $guarded = ['created_at', 'updated_at'];
    protected $primaryKey = "id_contrato";

    public function empleado()
    {
        return $this->belongsTo('freesgen\application\models\eloquent\User', 'id_empleado', 'user_id');
    }

    public function cliente()
    {
        return $this->belongsTo('freesgen\application\models\eloquent\Client', 'id_cliente', 'id_cliente');
    }

    public function payments()
    {
        return $this->hasMany('freesgen\application\models\eloquent\Payment', 'id_contrato', 'id_contrato');
    }

    public function count_payments($columnName = 'no pagado', $columValue = 'estado')
    {
        $this->payments->where($columnName, $columValue);
    }
}