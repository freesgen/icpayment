<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends My_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("user_model");
		$this->load->model("client_model");
		$this->load->model("service_model");
		$this->load->model("contract_model");
		$this->load->model("contract_view_model");
		$this->load->model("payment_model");
		$this->load->model("settings_model");
		$this->load->model("company_model");
		$this->load->model("report_model");
		$this->load->model("caja_chica_model");
		$this->load->model("averia_model");
		$this->load->model("section_model");
		$this->load->model("extra_model");
		$this->load->model("cancelations_model");
		$this->load->library('parser');
		$this->load->library('twig');
		// $this->output->enable_profiler(TRUE);
 }

	public function index($page = "login"){
		if(!isset($_SESSION['user_data'])):
			// $data['title'] = "login";
			// $this->load->view('pages/login',$data);
			$data  = $this->define_data($page, ['login']);
      $this->parser->parse('pages/login',$data);
		else:
			redirect(base_url('app/admin/home'));
		endif;

	}

	public function admin($page = 'home', $params = null){
		authenticate();
		auth_user_type_for_pages($page, 1, base_url('app/admin/home'));
    $this->session->set_flashdata('params', $params);
		$tooltip = $this->load->view('layouts/headertooltip', '', true);

		$data['title'] = $page;
		$data['tooltip'] = $tooltip;
		$data['id'] = $params;
		$data = array_merge($data, $this->get_global_data($page, $params));
		
		if (in_array($page, ['detalles'])) {
			$this->load_client($params);
		}
		
		$this->load->view('layouts/header',$data);
		$this->load->view("pages/$page", $data);
		$this->load_modals($page);
		$this->load->view('layouts/footer');
	}

	public function imprimir($page, $id = null){
		authenticate();
		$data['title'] = $page;
		$info = [];

		if ($page == 'cierre') {
			$this->load->model('caja_mayor');
			$info['info'] = $this->caja_mayor->cierres_report('summary');
    }

    if ($id) {
      $result = $this->load_receipt($id, $page);
      $page = $result['page'];
      $info = $result['info'];
    }
		$this->load->view('layouts/header_impresos',$data);
		$this->load->view("impresos/$page", $info);
  }

  private function load_receipt($id, $page) {
    $recibo_info = str_contain('_', $id) ? $this->payment_model->get_receipts($id): $this->payment_model->get_payment($id, true);
    if (str_contain('_', $id) && str_contain('abono', $recibo_info['concepto'])) {
      $recibo_info = null;
    } else if(str_contain('abono', $recibo_info['concepto'])){
      $page = 'recibo_abono';
    }

    $this->session->set_flashdata('recibo_info', $recibo_info);
    return ['info' => $recibo_info, 'page' => $page];
  }

	private function load_modals($page) {
		$modals = get_modals($page);
		if($modals != FALSE){
			foreach ($modals as $modal) {
				$this->load->view($modal);
			}
		}
	}

	private function load_client($id) {
		if ($id) {
			$data = $this->client_model->get_client($id);
			$_SESSION['client_data'] = $data;
			return $data;
		}
  }

	public function login(){
  	$nickname = $_POST['user'];
  	$password = $_POST['password'];

   	$is_correct = $this->user_model->login($nickname,$password);
		 if($is_correct){
			echo $is_correct;
		 }else{
			echo "Sus credenciales han sido incorrectas";
		 }
  }

	public function logout(){
		authenticate();
    session_unset($_SESSION['user_data']);
    session_destroy();
    redirect(base_url());
	}
	
	private function get_global_data($page, $params) {
    $data                  = $this->define_data($page, ['app'], ['app']);
    $data['user']          = $this->my_auth->get_user_data();
    $data['company']       = $this->company_model->get_empresa();
    $data['notifications'] = $this->report_model->count_moras_view();
    $data['params']        = $params;
    return $data;
  }

  private function define_data($title, $js = [] , $css = []) {
    $jsFiles  = [];
    $cssFiles = [];
    $js       = array_merge(['../lib/pace.min','manifest','vendor'], $js);
    $css      = array_merge(['secundaryCss.min', '5-others/square/frontend.min', 'main.min', 'mainv2'], $css);
    $assets   = 'assets/';

    foreach ($js as $filename) {
      array_push($jsFiles, ['link' => base_url()."{$assets}js/bundle/{$filename}.js"]);
    }

    foreach ($css as $filename) {
      array_push($cssFiles,['link' => base_url()."{$assets}css/{$filename}.css"]);
    }

    return [
      'title' => $title,
      'css'   => $cssFiles,
      'js'    => $jsFiles,
			'url'   => base_url(),
			'year' => date('Y')
    ];
	}
	
	
}
