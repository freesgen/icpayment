<?php
namespace freesgen\application\traits;


trait ReporterTrait {
    public function getReport($resourceId) {
        $report = $this->index($resourceId, 'report');
        $resource = $report->toArray();
		$this->session->set_flashdata($this->modelName, $resource);
		redirect(base_url($this->reportEndpoint));
    }

    public function template($vars, $title, $templateName) {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        \PhpOffice\PhpWord\Settings::setPdfRendererPath(APPPATH."../".'vendor/dompdf/dompdf');
        \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
  
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor("./assets/templates/$templateName.docx");
        foreach ($vars as $key => $value) {
          if ($key == 'rows') {
            $cont = 0;
            $templateProcessor->cloneRow('clientes.cont', count($value));
            foreach ($value as $row => $content) { 
              $cont++;
              foreach ($content as $col => $text) {
                $templateProcessor->setValue($title.".".$col."#$cont", $text);
              } 
            }
          } else {
            $templateProcessor->setValue($key, $value);
          }
          # code...
        }
        $filename = 'test.docx';
  
        $templateProcessor->saveAs($filename);
  
        $pdf = "test.pdf";
        Gears\Pdf::convert($filename, $pdf);
        // $phpWord = \PhpOffice\PhpWord\IOFactory::load($filename);
          // $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord,'PDF');
          // $xmlWriter->save($pdf);  // Save to PDF
  
        $salida = "Reporte de clientes" . date("d-m-Y") . ".pdf";
  
        header('Content-type: application/pdf');
        header("Content-Disposition: inline; filename=\"{$title}\"");
        readfile($pdf);
        unlink($filename); // deletes the temporary file
        unlink($pdf); // deletes the temporary file
        exit;
        die();
      }
  
}