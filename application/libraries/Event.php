<?php

  class Event
  {
      private $session;
      private $user;
      private $ci;

      public function __construct($event_model)
      {
          $this->user = isset($_SESSION['user_data']) ? $_SESSION['user_data']: [];
          $this->event_model = $event_model['event_model'];
          $this->context = $event_model['context'];
      }

      public function register($user_id, $message_type, $description, $link)
      {
          $event = [
            'id_usuario'    => $user_id ? $user_id : $this->user['user_id'] ?? null,
            'tipo'          => $message_type,
            'descripcion'   => $description,
            'titulo_enlace' => $link ? $link[0] : "n/d",
            'enlace'        => $link ? $link[1]: "",
          ];

          return $this->event_model->add($event);
      }

      public function trigger($event_name, $type, $params, $event_message='')
      {
          $params['event_type'] = $type;
          $params['event_message'] = $event_message;
          call_user_func([$this, $event_name."_event"], $type, $params);
      }

      public function client_event($type, $params)
      {
          $link = ['cliente', "process/details/{$params['id_cliente']}/"];
          $name = $this->get_client($params);
          $this->register(null, $type, "cliente $name {$params['event_message']}", $link);
      }

      public function contract_event($type, $params)
      {
          $link = ['contrato', "process/details/{$params['id_cliente']}/contratos"];
          $name = $this->get_client($params);
          $this->register(null, $type, "contrato codigo {$params['id_contrato']} del cliente $name {$params['event_message']}", $link);
      }


      public function extra_event($type, $params)
      {
          $link = ['extra', "process/details/{$params['id_cliente']}/extras"];
          $name = $this->get_client($params);
          $service = strtoupper($params['servicio']);
          $this->register(null, $type, "extra $service codigo {$params['id_extra']} | cliente $name {$params['event_message']}", $link);
      }

      public function payment_event($type, $params)
      {
          $link    = ['recibo', "process/getrecibo/{$params['id_pago']}"];
          $name    = $this->get_client($params);
          $service = strtoupper($params['servicio']);
          $info    = "Total pago: \$RD". CurrencyFormat($params['total']);

          if (str_contain('descuento', $params['event_message'])) {
              $info .= " | Descuento: \$RD". CurrencyFormat($params['descuento']);
              $info .=  " | Razon Descuento: {$params['razon_descuento']}";
          }

          if ($params['abono_a']) {
              $params['event_message'] = 'abono a';
          } else {
              $params['event_message'] .= ' a ';
          }

          $this->register(null, $type, "{$params['event_message']} $service del cliente $name | $info ", $link);
      }

      public function service_event($type, $params)
      {
          $link = ['servicio', "app/admin/servicios"];
          $this->register(null, $type, "servicio: {$params['nombre']}{$params['descripcion']} {$params['event_message']}", $link);
      }

      public function ticket_event($type, $params)
      {
          $link = ['averia', "app/admin/notificaciones/{$params['id_averia']}"];
          $name = $this->get_client($params);
          $this->register(null, $type, "averia - contrato #{$params['id_contrato']} - cliente $name {$params['event_message']}", $link);
      }

      public function comment_event($type, $params)
      {
          $link = ['averia', "app/admin/notificaciones/{$params['id_averia']}"];
          $name = $this->get_client($params);
          $this->register(null, $type, "reporte/comentario - averia #{$params['id_averia']} del contrato #{$params['id_contrato']} - cliente $name {$params['event_message']}", $link);
      }

      public function petty_cash_event($type, $params)
      {
          $link = ['caja chica', "app/admin/administrador"];
          $this->register(null, $type, "{$params['event_message']}", $link);
      }

      public function expense_event($type, $params)
      {
          $link   = ['gastos', "app/admin/informes"];
          $gasto  = $params['descripcion']." \$RD".CurrencyFormat($params['monto']);
          $this->register(null, $type, "gasto: $gasto {$params['event_message']}", $link);
      }

      public function background_event($type, $params)
      {
        $this->register(null, $type, "{$params['event_message']}", null);
      }

      public function closing_event($type, $params)
      {
          $link   = ['cierres', "app/admin/informes"];
          $info  = "total ingresos: \$RD".CurrencyFormat($params['total_ingresos'])." | Gastos \$RD ".CurrencyFormat($params['total_gastos']);
          $info  .= "| Banco: \$RD".CurrencyFormat($params['banco']);
          $this->register(null, $type, "cierre: $info {$params['event_message']}", $link);
      }


      public function free_space()
      {
          $this->event_model->get_excel_data(true);
          $this->event_model->free_space(2);
      }

      public function get_client($params)
      {
          return (isset($params['cliente'])) ? strtoupper($params['cliente']) : strtoupper($params['nombres']." ".$params['apellidos']);
      }
  }
