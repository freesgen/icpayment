<?php
namespace freesgen\application\models\eloquent;

class Price extends \Illuminate\Database\Eloquent\Model
{
    //
    protected $table = 'ic_precios';
    protected $guarded = ['created_at', 'updated_at'];
}