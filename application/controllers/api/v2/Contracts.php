<?php

defined('BASEPATH') or exit('No direct script access allowed');
use freesgen\application\controllers\My_Crud;
use freesgen\application\models\eloquent\Contract;
class Contracts extends My_Crud
{
    protected $modelName = 'Contract';
    // protected $listener = PaymentAgreementListener::class;
    protected $model = Contract::class;
    protected $fields = ['fecha', 'codigo'];

    public function unpaid_payments_count($id) {
        $queryParams   = $_SERVER['QUERY_STRING'];
        $queryParams = $this->getParams($queryParams);
        
        $modelQuery = $this->model->find($id);
        $results = $modelQuery->payments()->where('estado', 'no pagado')->count();
        $returnData = [
                'contract' => $modelQuery,
                'data' => $results,
        ];

        return $this->response_json($returnData);
    }
}