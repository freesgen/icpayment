import Vue from 'vue';
import jquery from 'jquery';
import VueTheMask from 'vue-the-mask';
import globals from './components/sharedComponents/globals';
import Store from './store/AppStore';
import ContractStore from './components/contratos/store/ContractStore';
import utils from './components/sharedComponents/utils';
import HomeSection from './components/home/HomeSection';
import ContractSection from './components/contratos/ContractSection.vue';
import DetailsSection from './components/detalles/DetailsSection.vue';
import NewContractSection from './components/contratos/NewContractSection.vue';
import ContractPaymentAgreementModal from './components/contratos/components/ContractPaymentAgreementModal.vue';
import ContractExtraModal from './components/contratos/components/ContractExtraModal.vue';
import pusherMixin from './pusherMixin';
import { Money } from 'v-money'
import {ServerTable, ClientTable, Event} from 'vue-tables-2';
import "./plugins/element";
window.appStore = new Store();
window.appBus = new Vue();
Vue.use(VueTheMask);
const MoneyConfig = {
  precision: 2,
  thousands: ',',
  decimal: '.',
  prefix: '',
  suffix: ''
};

globals(Vue);

window.Vue = Vue;
Vue.component('contract-section', ContractSection);
Vue.component('details-section', DetailsSection);
Vue.component('new-contract-section', NewContractSection);
Vue.component('contract-payment-agreement-modal', ContractPaymentAgreementModal);
Vue.component('contract-extra-modal', ContractExtraModal);
Vue.component('money', Money);
Vue.use(ServerTable);
Vue.use(ClientTable);

try {
  if (document.querySelector('#vue-dashboard')) {
    new Vue({
       el: '#vue-dashboard',
       mixins: [pusherMixin],
       components: {
         HomeSection,
         ContractSection
       },
     
       data: {
         store: window.appStore
       },
     
       mounted() {
         this.getCompany();
         this.getUser();
         this.getSaldo();
         this.getDayIncome();
         window.appStore = this.store;
         window.appBus.$on('caja::actualizada', () => {
           this.getSaldo();
         });
       },
     
       methods: {
         toggleMenu() {
           window.appBus.$emit('toggle-menu')
         },
         openPettyCash(mode) {
           this.store.setPettyCashMode(mode);
         },
     
         saveTransaction() {
           const empty = utils.isEmpty(this.store.pettyCashTransaction);
           if (!empty) {
             this.$http.post(`petty_cash/${this.store.pettyCashMode}`, this.getDataForm(this.store.pettyCashTransaction))
               .then((res) => {
                 this.showMessage(res.data.message);
                 if (res.data.message.type === 'success') {
                   this.store.pettyCashTransactionEmpty();
                   $('#app-petty-cash-modal').modal('hide');
                 }
                 this.getSaldo();
                 window.appBus.$emit('transaction');
                 if (this.store.pettyCashMode === 'edit') {
                   $('#app-petty-cash-modal').modal('hide');
                 }
               });
           } else {
             this.$toasted.error('Revise y LLene todos los campos por favor');
           }
         },
     
         getSaldo() {
           this.$http.get('petty_cash/get_balance')
             .then((res) => {
               this.store.setPettyCashBalance(res.data);
             });
         },
     
         getCompany() {
           this.$http.get('company/get')
             .then((res) => {
               this.store.setCompany(res.data);
             });
         },
     
         getDayIncome() {
           this.$http.get('report/get_day_income')
             .then((res) => {
               this.store.setDayIncome(res.data.income);
             });
         },
     
         getUser() {
           this.$http.get('user/get_user')
             .then((res) => {
               this.store.setUser(res.data.user);
             });
         }
     
       }
     });
  }

  if (document.querySelector('#footer-vue-app')) {
    new Vue({
        el: '#footer-vue-app',    
        mounted() {

        },
    
        data() {
          const store = new ContractStore();
          return {
            store,
            title: 'Contratos',
            parentId: '#contract-table-container',
            toolbar: '#contract-toolbar',
            contracts: '',
            filter: 'activo',
            tableOptions: {
              pageSize: 200,
              pageList: [50, 100, 200, 500, 1000],
              states: ['activo', 'saldado', 'suspendido', 'cancelado'],
              stateField: 'estado'
            },
            columns: ['codigo', 'fecha', 'age'],
            toptions: {
                // see the options API
            },
            selectedContract: null,
            options: [
              { key: 'activo', text: 'Activos' },
              { key: 'saldado', text: 'Saldados' },
              { key: 'suspendido', text: 'Suspendidos' },
              { key: 'cancelado', text: 'Cancelados' },
            ]
          };
        },
    
        computed: {
          cols() {
            return this.store.columns;
          },
        },
    
        methods: {
          getContract(modal) {
            const contract = this.selectedContract;
            if (contract) {
              this.$http.post('contract/get_contract', this.getDataForm({ id: contract.id }))
                .then((res) => {
                  this.store.setContract(res.data.contract);
                  this.store.setContractMode('update');
                  $(modal).modal();
                })
                .catch((err) => {
                  this.$toasted.error(err);
                });
            } else {
              this.$toasted.info('seleccione un contrato primero');
            }
          },
    
          listen(name, row) {
            this.selectedContract = row;
          },
    
          callModal(modalMiddleName) {
            const contract = this.selectedContract;
            if (contract) {
              $(`#contract-${modalMiddleName}-modal`).modal();
            } else {
              this.$toasted.info('seleccione un contrato primero');
            }
          },
  
        }
      })
  }
} catch (e) {
  console.log("main component failed")
}
