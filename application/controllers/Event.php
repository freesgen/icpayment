<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("event_model");
	}

	public function get($full = false){
		authenticate();
		$is_admin =  auth_user_type(0);
		$data = $this->get_post_data('data');

		if ($data && $is_admin) {
			$res  = $this->event_model->get($data['text'], $data['first_date'], $data['second_date']);
			$this->response_json($res);
		}
  }

  public function get_report() {
		authenticate();
    $report = $this->event_model->get_excel_data();

		if(!$report) echo "No hay datos";
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

    $file = "reporte_de_eventos.xlsx";
		$myreport_sheet = create_excel_file($report, "Resumen Eventos ". date('d-m-Y'), "Reporte de enventos");
    download_excel_file($myreport_sheet, $file);
  }

  public function free_space() {
    $this->event_model->free_space(2, '2018-05-29');
  }
}
