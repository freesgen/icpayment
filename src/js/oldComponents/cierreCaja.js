if (isCurrentPage("cierre")) {
  (function () {
    var totales = {
      total1: 0,
      total5: 0,
      total10: 0,
      total20: 0,
      total25: 0,
      total50: 0,
      total100: 0,
      total200: 0,
      total500: 0,
      total1000: 0,
      total2000: 0
    }

    var gasto = {
      'fecha': '',
      'descripcion': '',
      'monto': '',
    }

    var gastos = [{
      fecha: now(),
      descripcion: "hola",
      monto: 2000,
      id_gasto: 1
    }]
    var autor = $('#autor-cierre').text().trim()

    var appCierre = new Vue({
      el: '#app-cierre',
      data: {
        isHide: false,
        fecha: now(),
        data_cierre: {
          autor: autor,
          pagos_facturas: 0,
          pagos_extras: 0,
          pagos_efectivo: 0,
          pagos_banco: 0,
          total_ingresos: 0,
          efectivo_caja: 0,
          total_descuadre: 0,
          total_gastos: 0,
          banco: 0
        },
        conteo: totales,
        suma: 0,
        gasto: gasto,
        gastos: gastos
      },

      mounted: function () {
        this.getGastos();
        this.setIngresos();
      },

      created: function () {
        $('.will-load').css({
          visibility: "visible"
        })
      },

      filters: {
        currencyFormat: function (number) {
          return CurrencyFormat(number);
        }
      },

      methods: {
        changeTotal: function (e) {
          var unit = e.srcElement.attributes['data-unit'].value
          var cantidad = e.srcElement.value
          var total = cantidad * unit
          totales['total' + unit] = cantidad * unit * 1.00
        },

        addGasto: function (e) {
          var gasto = this.gasto;
          gasto.fecha = now();
          var form = 'data=' + JSON.stringify(gasto);
          var send = axios.post(BASE_URL + 'caja/add_gasto', form)
          send.then(function (response) {
            var data = response.data
            displayMessage(data.mensaje)
            appCierre.fillGastos(data.gastos, "normal")
            appCierre.setGastoTotal(data.total_gastos)
          });
          send.catch(function () {
            console.log(error);
          });
        },

        fillGastos: function (gastos_servidor, mode) {
          if (mode == "group") {
            if (gastos_servidor != null || gastos_servidor.length > 0) {
              appCierre.gastos = gastos_servidor;
            } else {
              appCierre.gastos = [];
            }
          } else {
            appCierre.gastos.push(JSON.parse(gastos_servidor)[0]);
          }
        },

        setGastoTotal: function (totalGastos) {
          this.data_cierre.total_gastos = totalGastos
        },

        getGasto: function (e) {
          var gasto = this.gasto;
          var form = 'data=' + JSON.stringify(gasto);
          connectAndSend('caja/get_gasto', false, null, appCierre.fillGastos, form, null, null);
        },

        deleteGasto: function (e) {
          var caller = e.target;
          if (caller.localname == "i") {
            caller = caller.parentElement;
          }
          var id = caller.attributes['data-id'].value
          swal({
            title: 'Está Seguro?',
            text: "Seguro de que quiere eliminar este gasto?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Estoy Seguro!',
            cancelButtonText: 'Cancelar'
          }).then(function () {
            var form = 'data=' + JSON.stringify({
              id: id,
              fecha: now()
            })
            var send = axios.post(BASE_URL + 'caja/delete_gasto', form)
            send.then(function (response) {
              var data = response.data
              displayMessage(data.mensaje)
              appCierre.fillGastos(data.gastos, "group")
              appCierre.setGastoTotal(data.total_gastos)
            });
            send.catch(function (error) {

            });
          });
        },

        getGastos: function () {
          var data = {
            fecha: now()
          }
          var form = 'data=' + JSON.stringify(data)
          var send = axios.post(BASE_URL + 'caja/get_gastos', form)
          send.then(function (response) {
            var data = response.data
            displayMessage(data.mensaje)
            appCierre.fillGastos(data.gastos, "group")
            appCierre.setGastoTotal(data.total_gastos)
          });
          send.catch(function () {
            console.log(error);
          })
        },

        setIngresos: function () {
          var form = 'data=' + JSON.stringify({
            fecha: now()
          })
          var self = this.data_cierre;
          var send = axios.post(BASE_URL + 'caja/get_ingresos', form)
          send.then(function (response) {
            var data = response.data
            self.pagos_facturas = data.pagos_facturas;
            self.pagos_extras = data.pagos_extras;
            self.pagos_efectivo = data.pagos_efectivo;
            self.pagos_banco = data.pagos_banco;
            self.total_ingresos = parseFloat(data.pagos_facturas) + parseFloat(self.pagos_extras);
            self.total_descuadre = -self.pagos_efectivo + self.efectivo_caja;
          });
          send.catch(function () {
            console.log(error);
          })
        },

        cerrarCaja: function () {
          var self = this;
          var cierre = this.data_cierre;
          window.cierre = cierre;
          if (cierre.total_descuadre != 0) {
            swal({
              title: 'Está Seguro?',
              text: "Hay un descuadre en la caja, quiere hacer el cierre de todos modos?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Si',
              cancelButtonText: 'No'
            }).then(function () {
              self.cerrar(cierre)
            })
          } else {
            self.cerrar(cierre);
          }
        },

        cerrar: function (cierre) {

          cierre.fecha = now();
          var form = 'data=' + JSON.stringify(cierre);
          var send = axios.post(BASE_URL + 'caja/add_cierre', form)
          send.then(function (response) {
            var data = response.data
            displayMessage(data.mensaje)
            self.isHide = true;
            appSummaryView.isHide = false;
            appSummaryView.cierre = cierre;
            $("#app-cierre").addClass('hide');
            $(".top-nav").addClass('hide');
            $("#print-view").css({
              visibility: "visible"
            })

          });
          send.catch(function () {
            console.log(error);
          });
        }
      },

      computed: {
        getTotal: function (e) {
          var t = totales;
          var self = this.data_cierre;
          var suma = sumar([t.total1, t.total5, t.total10, t.total20, t.total25, t.total50, t.total100, t.total200, t.total500, t.total1000, t.total2000]);
          this.suma = suma;
          self.efectivo_caja = suma.toFixed(2);
          self.total_descuadre = parseFloat(-self.pagos_efectivo) + parseFloat(self.efectivo_caja);
          self.banco = parseFloat(self.pagos_banco) + parseFloat(self.pagos_efectivo) - parseFloat(self.total_gastos) + parseFloat(self.total_descuadre)
          return this.suma;
        },

        decimals: function () {
          var fields = ["pagos_facturas", "pagos_extra", "pagos_efectivo", "pagos_banco", "total_ingresos", "efectivo_caja", "total_descuadre", "total_gasto", "banco"];
          fields.forEach(function (field) {
            this.data_cierre[field] = this.data_cierre[field].toFixed(2)
          }, this);
        }
      }
    })

    function sumar(valores) {
      var suma = 0;
      for (var i = 0; i < valores.length; i++) {
        suma += parseFloat(valores[i]);
      }
      return suma;
    }

    function now() {
      return moment().format("YYYY-MM-DD");
    }

    var appSummaryView = new Vue({
      el: "#print-view",
      data: {
        isHide: true,
        back: {
          link: "somelink",
          text: "volver a cierre"
        },
        foward: {
          link: BASE_URL + "app/logout",
          text: "cerrar session"
        },
        cierre: {
          autor: '',
          pagos_facturas: 0,
          pagos_extras: 0,
          pagos_efectivo: 0,
          pagos_banco: 0,
          total_ingresos: 0,
          efectivo_caja: 0,
          total_descuadre: 0,
          total_gastos: 0,
          banco: 0
        }
      },
      filters: {
        currencyFormat: function (number) {
          return "RD$ " + CurrencyFormat(number);
        },

        spanishDateFormat: function (date) {
          moment.locale('es-DO');
          return moment(date).format('dddd DD [de] MMMM [del] YYYY')
        }
      },
      methods: {
        goBack: function () {
          appSummaryView.isHide = true;
          appCierre.isHide = false;
          self.isHide = true;
          $(".top-nav").removeClass('hide');
          $("#app-cierre").removeClass('hide');
        },
        print: function () {
          print()
        }
      }
    })
  })()
}