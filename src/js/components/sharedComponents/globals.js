import swal from 'sweetalert2';
import axios from 'axios';
import Toasted from 'vue-toasted';
import utils from './utils';
import Pusher from 'pusher-js';

export default (Vue) => {
  const options = {
    theme: 'toasted-primary',
    position: 'top-right',
    duration: 5000
  };

  Vue.use(Toasted, options);

  Vue.mixin({
    mounted() {
      window.document.onreadystatechange = () => {
        this.quitSplash();
      };
    },

    methods: {
      showMessage(message) {
        this.$toasted[message.type](message.text);
      },

      getDataForm(object) {
        return `data=${JSON.stringify(object)}`;
      },

      quitSplash() {
        const state = document.readyState;
        const splash = document.querySelector('.splash-screen');
        if (state === 'complete') {
          try {
            splash.classList.add('hide');
            document.querySelector('header').classList.remove('loading');
          } catch (e) {
            
          }
        }
      },

      deleteConfirmation(title, message, confirmText = 'Eliminar', cancelText = 'Cancelar') {
        return this.$confirm(message, title, {
          confirmButtonText: confirmText,
          cancelButtonText: cancelText,
          type: 'warning'
        })
      }
    },

    filters: {
      currencyFormat(number) {
        return utils.CurrencyFormat(number);
      },

      phoneFormat(tel) {
        if (tel.length === 10) {
          return `(${tel.slice(0, 3)}) ${tel.slice(3, 6)}-${tel.slice(6)}`;
        }
        return tel;
      }
    }
  });

  const $http = axios.create({
    baseURL: baseURL + 'api/v2/'
  });

  Pusher.logToConsole = true;

  const $pusher = new Pusher('ee555a108dfa45ecebf4', {
    cluster: 'us2',
    forceTLS: true
  });

  Vue.prototype.$http = $http;
  Vue.prototype.$pusher = $pusher;
};
