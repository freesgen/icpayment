window.Clients = {
  add: function () {
    var form, nombres, apellidos, cedula, celular, provincia, sector, calle, casa,detallesDireccion,
        telefono,lugarTrabajo, telTrabajo, ingresos, fechaRegistro, estado;

    nombres            = $("#client-name").val();
    apellidos          = $("#client-lastname").val();
    cedula             = getVal($("#client-dni"));
    celular            = getVal($("#client-phone"));
    provincia          = $("#client-provincia").val();
    sector             = $("#client-sector").val();
    calle              = $("#client-street").val();
    casa               = $('#client-house').val();
    detallesDireccion  = $('#client-direction-details').val();
    telefono           = getVal($('#client-telephone'));
    lugarTrabajo       = $('#client-job').val();
    telTrabajo         = getVal($('#client-job-telephone'));
    ingresos           = $('#client-salary').val();
    fechaRegistro      = moment().format("YYYY-MM-DD");
    estado             = "no activo";

    var is_empty = isEmpty([nombres, apellidos, cedula, celular, provincia, sector, calle, casa, telefono]);
    if (!is_empty) {
      form = 'nombres=' + nombres + "&apellidos=" + apellidos + "&cedula=" + cedula + "&celular=" + celular;
      form += "&provincia=" + provincia + "&sector=" + sector + "&calle=" + calle + "&casa=" + casa + "&telefono=" + telefono;
      form += "&lugar_trabajo=" + lugarTrabajo + "&tel_trabajo=" + telTrabajo + "&ingresos=" + ingresos + "&fecha_registro=" + fechaRegistro;
      form += "&estado=" + estado + "&detalles_direccion=" + detallesDireccion  + "&tabla=clientes";

      connectAndSend("process/add", true, initClientHandlers, null, form, Clients.getAll);
    } else {
      displayAlert("Revise", "LLene todos los campos por favor", "error");
    }
  },

  getAll: function () {
    var form = "tabla=clientes";
    connectAndSend('process/getall', false, initClientHandlers, clientTable.refresh, form, null);
  },

  getOne: function (id, receiver) {
    form = "tabla=clientes&id=" + id;
    connectAndSend("process/getone", false, initClientHandlers, receiver, form, null)
  },

  receiveForEdit: function (content) {
    var client            = JSON.parse(content);
    this.id                = client['id_cliente'];
    var $nombres           = $("#u-client-name");
    var $apellidos         = $("#u-client-lastname");
    var $cedula            = $("#u-client-dni");
    var $celular           = $("#u-client-phone");
    var $estado            = $("#u-client-estado");
    var $diasGracia        = $("#u-client-dias-gracia");
    var $provincia         = $("#u-client-provincia");
    var $sector            = $("#u-client-sector");
    var $calle             = $("#u-client-street");
    var $casa              = $('#u-client-house');
    var $detallesDireccion = $('#u-client-direction-details');
    var $telefono          = $('#u-client-telephone');
    var $lugarTrabajo      = $('#u-client-job');
    var $telTrabajo        = $('#u-client-job-telephone');
    var $ingresos          = $('#u-client-salary');

    $nombres.val(client['nombres']);
    $apellidos.val(client['apellidos']);
    $cedula.val(client['cedula']);
    $celular.val(client['celular']);
    $estado.val(client['estado']);
    $diasGracia.val(client['dias_de_gracia']);
    $provincia.val(client['provincia']);
    $sector.val(client['sector']);
    $calle.val(client['calle']);
    $casa.val(client['casa']);
    $detallesDireccion.val(client['detalles_direccion']);
    $telefono.val(client['telefono']);
    $lugarTrabajo.val(client['lugar_trabajo']);
    $telTrabajo.val(client['tel_trabajo']);
    $ingresos.val(client['salario']);

    $("#update-client-modal").modal();
    $("#btn-update-client").on('click', function () {
      updateClient();
    });

    function updateClient() {
      var is_empty = isEmpty([$nombres.val(), $apellidos.val(), $cedula.val(), $celular.val(), $provincia.val(), $sector.val(), $calle.val(),
        $casa.val(), $telefono.val()
      ]);

      if (!is_empty) {
        form = 'id=' + id + '&nombres=' + $nombres.val() + "&apellidos=" + $apellidos.val() + "&cedula=" + getVal($cedula);
        form += "&celular=" + getVal($celular) + "&provincia=" + $provincia.val() + "&sector=" + $sector.val() + "&calle=" + $calle.val();
        form += "&casa=" + $casa.val()+ "&detalles_direccion=" + $detallesDireccion.val()  + "&telefono=" + getVal($telefono) + "&lugar_trabajo=" + $lugarTrabajo.val() + "&tel_trabajo=";
        form += getVal($telTrabajo) + "&tabla=clientes" + "&dias_de_gracia=" + $diasGracia.val() + "&estado=" + $estado.val();
        form += "&ingresos=" + $ingresos.val();

        connectAndSend("process/update", true, initClientHandlers, null, form, Clients.getAll);

      } else {
        displayAlert("Revise", "LLene todos los campos por favor", "error");
      }
    }
  },

  saveObservations: function () {
    var form, observations,idCliente;

    observations = $("#text-observations").val();
    idCliente    = $("#detail-client-id").val();

    form = 'observaciones=' + observations + "&tabla=observaciones&id_cliente=" + idCliente;
    connectAndSend("process/update", true, null, null, form, null)
  }
}
