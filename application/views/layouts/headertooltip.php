<?php $user_data = get_user_data() ?>

<ul class="dropdown-menu" aria-labelledby="dLabel">
  <li><p><a href="<?php echo base_url('app/admin/cuenta')?>"><i class="material-icons">person_pin</i><b><?php echo $user_data['fullname']."<br>".$user_data['typestr'] ?></b></a></p></li>
  <li><a href="<?php echo base_url('app/admin/cierre')?>"><i class="material-icons">lock_open</i>Cerrar Caja</a></li>
  <?php if(auth_user_type(0)): ?>
  <li class="hidden-xs"><a href="<?php echo base_url('app/admin/administrador')?>">
      <i class="material-icons">settings</i>Configuración</a>
  </li>
  <?php endif;?>
  <li><a href="<?php echo base_url('app/logout')?>">
      <i class="material-icons">power_settings_new</i> Cerrar Sesión</a>
  </li>
</ul>
