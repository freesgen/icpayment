<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Process extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model("client_model");
		$this->load->model("service_model");
		$this->load->model("contract_model");
		$this->load->model("contract_view_model");
		$this->load->model("payment_model");
		$this->load->model("company_model");
		$this->load->model("report_model");
		$this->load->model("settings_model");
		$this->load->model("averia_model");
		$this->load->model("caja_chica_model");
		$this->load->model("section_model");
		$this->load->model("extra_model");
		$this->load->model("aditional_service_model");
		$this->load->model("cancelations_model");
		$this->load->model("price_model");
	}

	public function add() {
		authenticate();
		$data = $_POST;
		$tabla = $_POST['tabla'];
		switch ($tabla) {
			case "clientes":
				if ($client = $this->client_model->add($data)) {
					$this->event->trigger('client', 1, $client);
				}
				break;
			case "servicios":
				if ($this->service_model->add($data)) {
          $this->event->trigger('service', 1, $data);
				}
				break;
			// DEPRECATED: delete in the next version
			case "averias":
				if ($this->averia_model->add($data)) {
          $contract = $this->contract_model->get_contract_view($data['id_contrato']);
          $this->event->trigger('ticket', 1, $contract, " Descripcion: {$data['descripcion']}");
        }
				break;
			case "caja":
				if ($data['entrada'] <= 0) {
					$this->set_message('El monto está en cero o es menor', 'error');
				} else {
					if ($this->caja_chica_model->add_money($data)) {
						$this->event->trigger('petty_cash', 3, [], "entrada en caja chica de \$RD". CurrencyFormat($data['entrada']));
					}
				}
				break;
			case "secciones":
				$is_saved = $this->section_model->add($data);
				switch ($is_saved) {
					case -1:
						echo MESSAGE_INFO." Este sector o codigo ha sido guardado anteriormente";
						break;
					case 0:
						echo MESSAGE_ERROR." No se ha podido Guardar el sector";
						break;
					case 1:
						$section_id = $this->section_model->get_section_id($data['codigo_area']);
						create_ips($section_id,$data);
						break;
				}
				break;
			case "contratos":
        $this->db->trans_start();
        if ($data['id_precio'] == 0) {
          $data['id_precio'] = null;
				}

				if ($data['codigo']) {
						$this->db->where('codigo', $data['codigo'])->select('ip_final');
						$result = $this->db->get('v_ips',1);
						$data['ip'] = $result->row_array()['ip_final'];
				}

        if ($contract_id = $this->contract_model->add($data)) {
          $this->client_model->is_active(true, $data);
          create_payments($contract_id, $data, $this);
          $this->section_model->update_ip_state($data['codigo'],'ocupado');
          $this->contract_model->update_amount($contract_id);
        }

        $this->db->trans_complete();

        if ($this->db->trans_status()){
          $res['mensaje']     =  MESSAGE_SUCCESS." Nuevo contrato agregado con exito";
          $res['tabla_pagos'] = $this->payment_model->list_all_of_contract($contract_id);
          // event
          $contract = $this->contract_model->get_contract_view($contract_id);
          $this->event->trigger('contract', 1, (array) $contract);
        } else {
          $this->db->trans_rollback();
          $res['mensaje'] = MESSAGE_ERROR." El Contrato No Pudo ser guardado";
          $res['tabla_pagos'] = null;
        }
        echo json_encode($res);
        break;
		}

	}

	public function getjson() {
		$data = json_decode($_POST['data']);
		$data = json_decode($_POST['data'],true);
		$action = $_POST['action'];
		$module = $_POST['module'];
      	switch ($action) {
			case 'update':
          		if ($client = $this->client_model->update($data)) {
					$this->event->trigger('client', 2, $client, "Nuevo estado: {$client['estado']}");
				  }
			break;
      }
  	}

	public function update() {
		authenticate();
		$data  = $_POST;
		$tabla = $_POST['tabla'];
		switch ($tabla) {
			case "clientes":
				if ($client = $this->client_model->update_client($data)) {
					$this->event->trigger('client', 2, $client);
				}
				break;
			case "observaciones":
				if ($client = $this->client_model->update_observations($data)) {
					$this->event->trigger('client', 2, $client, " Observacion: {$data['observaciones']}");
				}
				break;
			case "abonos":
				if (!$this->is_day_closed()) {
					$this->db->trans_start();
					$id_abono = set_abono($data,$this);
					$this->db->trans_complete();
					if ($this->db->trans_status() === false){
						$this->db->trans_rollback();
						echo MESSAGE_ERROR." No se pudo completar el abono";
					} else {
						$receipt = $this->payment_model->get_payment($id_abono, true);
          				$this->event->trigger('payment', 3, $receipt, "abono");
					}
				}
				break;

			case "servicios":
				$this->db->trans_start();
				$result = $this->service_model->update_service($data);
				$this->db->trans_complete();
				if ($this->db->trans_status() === false) {
					$this->db->trans_rollback();
					echo MESSAGE_ERROR." No pudo completarse la accion correctamente";
		    } else {

					echo " proceso completo";
					return ($result !== 'same') ? $this->event->trigger('service', 2, $data, " precio \$RD".CurrencyFormat($data['mensualidad'])): null;
				}
				break;
			case "pagos":
				$no_paid = $this->payment_model->check_for_update($data['id']);
				if (!$this->is_day_closed() && $no_paid) {
					$id_contrato = $data['id_contrato'];
					$this->db->trans_start();
					refresh_contract($id_contrato, $this, $data);
					$this->db->trans_complete();
					if ($this->db->trans_status() !== false) {
						// event
						$receipt = $this->payment_model->get_payment($data['id'], true);
						$this->event->trigger('payment', 3, $receipt, "pago");
					}
				} else {
					$message = (!$no_paid) ? 'Este pago ya ha sido realizado' : 'dia cerrado';
					echo MESSAGE_INFO." $message";
				}
				break;

			case "pagos_al_dia":
				if(!$this->is_day_closed()){
					$this->db->trans_start();
					payments_up_to_date($data);
					$this->db->trans_complete();
					if($this->db->trans_status() === false){
							$this->db->trans_rollback();
						echo MESSAGE_ERROR." No pudo completarse la accion correctamente";
					}else{
						echo " Proceso Completo";
					}
				}
				break;

			case "deshacer_pago":
			  if(!$this->is_day_closed()) {
					$no_paid = $this->payment_model->check_for_update($data['id_pago']);
					$is_admin =  auth_user_type(0);

					if(!$no_paid && $is_admin) {
						$this->db->trans_start();
						cancel_payment($data['id_pago'],$this);
						$this->db->trans_complete();
						if ($this->db->trans_status() === false){
							$this->db->trans_rollback();
							echo MESSAGE_ERROR." No Pudo deshacerse el Pago";
						}
					} else {
						$message = (!$is_admin) ? 'Debe ser administrador para dehacer el pago' : 'Este pago no ha sido realizado para deshacerse';
						echo MESSAGE_INFO." $message";
					}
				}
        break;

			case "discount_pagos":
			  if (!$this->is_day_closed()) {
					$no_paid = $this->payment_model->check_for_update($data['id_pago']);
					if($no_paid){
						$this->db->trans_start();
						payment_discount($data,$this);
						$this->db->trans_complete();
						if($this->db->trans_status() === false){
							$this->db->trans_rollback();
							echo MESSAGE_ERROR." error en la operacion";
						}else{
							$receipt = $this->payment_model->get_payment($data['id_pago'], true);
          					$this->event->trigger('payment', 3, $receipt, "pago con descuento");
							echo " Proceso Completo";
						}
					}
				}
				break;
			case "empresa":
				$this->company_model->update($data);
				break;
			case "settings":
				$this->settings_model->update_settings($data);
				break;
			case "averias":
				if ($this->averia_model->update($data['id_averia'])) {
          $ticket = $this->averia_model->get_averia($data['id_averia']);
          $this->event->trigger('ticket', 2, $ticket, " nuevo estado: {$ticket['estado']}");
        }
				break;
			case "instalaciones":
				$this->report_model->update_installation($data['id_pago']);
				break;
			case "contratos":
				$event_message   = "";
				$this->db->trans_start();
				$contract = $this->contract_model->get_contract_view($data['id_contrato']);

				$data_for_update = [
					'nombre_equipo' => $data['nombre_equipo'],
					'mac_equipo'		=> $data['mac_equipo'],
					'router'				=> $data['router'],
					'mac_router'    => $data['mac_router'],
					'modelo'				=> $data['modelo'],
					'dia_pago'			=> $data['dia_pago']
				];

				if(isset($data['codigo'])){
						$this->section_model->update_ip_state($contract['codigo'],'disponible');
						$data_for_update['ip']     = $data['ip'];
						$data_for_update['codigo'] = $data['codigo'];
						$event_message             = "nuevo codigo IP {$data['codigo']}";
						$this->section_model->update_ip_state($data['codigo'],'ocupado');
				}

				if ($this->contract_model->update($data_for_update, $data['id_contrato'], true)) {
					if ($contract['dia_pago'] != $data['dia_pago']) {
						$this->payment_model->update_payment_day_contract($data['id_contrato'], $data['dia_pago']);
					}
				}

				$this->db->trans_complete();

        if ($this->db->trans_status() === false){
					echo "maldita sea";
          $this->db->trans_rollback();
				} else {
					$this->event->trigger('contract', 2, $contract, $event_message);
        }
				break;

		}
	}

	public function axiosupdate() {
		authenticate();
		$data   = json_decode($_POST['data'], true);
		$info   = json_decode($_POST['extra_info'], true);
		$response['mensaje'] = '';

		switch ($info['module']) {
			case 'pagos':
				if($data['tipo'] == '') $data['tipo'] = 'efectivo';

				if($this->payment_model->update($data, $info['id'])){
					$response['mensaje'] = MESSAGE_SUCCESS." procesando cambios";
					echo json_encode($response);
				}
				break;
			case 'ip':
				if ($this->section_model->update_ip_state($data['codigo'],$data['estado'])) {
					$response['mensaje'] = MESSAGE_SUCCESS." Estado de ip cambiado";
					echo json_encode($response);
				}
		}
	}

	public function retire() {
		authenticate();
		if ($data = $_POST) {
			$balance = $this->caja_chica_model->get_last_saldo();
			if ( $data['salida'] > $balance || $data['salida'] <= 0) {
				echo MESSAGE_ERROR.' El registro de salida que intenta ingresar es mayor a balance actual o menor a cero';
			} else {
				if ($this->caja_chica_model->retire_money($data)) {
					$this->set_message('Monto registrado');
					$this->event->trigger('petty_cash', 3, [], "retiro en caja chica de \$RD ". CurrencyFormat($data['salida']));
				}
			}
		}
	}

	public function upgrade() {
		authenticate();
		$data = $_POST;
		if (($result = upgrade_contract($this, $data)) && $result !== 'same') {
			$service   = $this->service_model->get_service($data['id_servicio']);
			$contract  = $this->contract_model->get_contract_view($data['id_contrato']);
			$this->event->trigger('contract', 2, $contract, "Cambio a plan {$service['nombre']} y precio {$data['cuota']}");
		}
	}

	public function getall() {
		authenticate();
		$tabla = $_POST['tabla'];
		switch ($tabla) {
			case "users":
				$this->user_model->get_all_users();
				break;
			case "clientes":
				$this->client_model->get_all_clients();
				break;
			case "servicios":
				$this->service_model->get_all_services();
				break;
			case "contratos":
				$this->contract_view_model->get_contract_view('activo');
				break;
			case "contratos_cliente":
				$this->contract_model->get_all_of_client($_POST['id']);
				break;
			case "pagos":
				$this->payment_model->get_all_of_contract($_POST['id']);
				break;
			case "v_proximos_pagos":
				$this->payment_model->get_next_payments($_POST);
				break;
			case "v_pagos_pendientes":
				$this->payment_model->get_moras_home();
				break;
			case "averias":
				$this->averia_model->get($_POST['estado']);
				break;
			case "instalaciones":
				$this->report_model->get_installations_list($_POST['estado']);
				break;
			case "ips":
				$this->section_model->get_all_of_section($_POST['id']);
				break;
			case "ip_list":
				$this->section_model->get_ip_list_of_section($_POST['id_seccion']);
				break;
			case "secciones":
				$this->section_model->get_sections_dropdown();
				break;
			case "caja":
				$this->caja_chica_model->get_rows();
				break;
		}
	}

	public function getlist() {
		authenticate();
		$tabla = $_POST['tabla'];
		if($tabla == "pagos"){
				$id_contrato = $this->contract_model->get_last_id();
				echo $this->payment_model->list_all_of_contract($id_contrato);
		}
	}

	public function getone() {
		authenticate();
		$tabla = $_POST['tabla'];
		switch ($tabla) {
			case "clientes":
				$result = $this->client_model->get_client($_POST['id'],true);
				if($result){
					 $dataJson = json_encode($result);
					 echo $dataJson;
				}else{
					echo "nada";
				}
				break;
			case "contratos":
				$result = $this->contract_model->get_contract_view($_POST['id_contrato'], true);
				if($result){
					 $dataJson = json_encode($result);
					 echo $dataJson;
				}else{
					echo $this->db->last_query();
				}
				break;
			case "pagos":
				$result['pago'] 		= $this->payment_model->get_payment($_POST['id_pago']);
				$result['settings'] = $this->settings_model->get_settings();
				if($result){
					echo json_encode($result);
				}else{
					echo "nada";
				}
				break;
			case "caja":
				$result = $this->caja_chica_model->get_last_saldo();
				echo $result;
				break;
		}
	}

	public function delete() {
		authenticate();
		$id = $_POST['id'];
		$tabla = $_POST['tabla'];

		switch ($tabla) {
			case 'clientes':
				if ($client = $this->client_model->delete_client($id)) {
          	$this->event->trigger('client', 4, (array) $client);
				}
				break;
			case 'servicios':
				$service = $this->service_model->get_service($id);

				if ($this->service_model->delete_service($id)) {
					$this->event->trigger('service', 4, $service);
				}

				break;
		}
	}

	public function count() {
		authenticate();
		$tabla = $_POST['tabla'];
		switch ($tabla) {
			case 'clientes':
				$this->client_model->count_clients();
				break;
			case 'contratos':
				$this->contract_view_model->count_contracts();
				break;
			case 'servicios':
				$this->service_model->count_services();
				break;
			case 'pagos_por_contratos':
				$this->payment_model->count_of_contract();
			case 'averias':
				$this->averia_model->count();
				break;
		}
	}

	public function search() {
		authenticate();
		if(isset($_POST['tabla'])){
			$data = $_POST;
			$tabla = $data['tabla'];
			if(isset($_POST['word'])){
				$word = $_POST['word'];
			}
		}else{
			$query = $_GET['q'];
			$tabla = $_GET['tabla'];
		}

		switch ($tabla) {
			case 'clientes':
				$clients = $this->client_model->search_clients($word);
				if ($clients)
				echo make_client_table($clients, 0);
				break;
			case 'clientes_para_averias':
				$res['items'] = $this->client_model->search_clients_for_message($query,'id_cliente');
				echo json_encode($res);
				break;
			case 'servicios':
				 $this->service_model->search_services($word);
				break;
			case 'v_contratos':
				 $this->contract_view_model->search_contracts($word);
				break;
			case 'caja':
				 $this->caja_chica_model->search_in_rows($data['id_empleado'],$data['fecha']);
				break;
		}
	}

	public function details($id, $active_window = "pagos") {
		authenticate();
		$_SESSION['client_data'] = $this->client_model->get_client($id);
		$this->session->set_flashdata('active_window',$active_window);
		redirect(base_url("app/admin/detalles/$id"));
	}

	public function newcontract($id) {
		authenticate();
		$_SESSION['client_data'] = $this->client_model->get_client($id);
		redirect(base_url('app/admin/nuevo_contrato'));
	}

	public function cancel() {
		authenticate();
		if (!$this->is_day_closed()){
			$data = $_POST;
			$pendents = $this->contract_view_model->get_pendent_payments($data['id_contrato']);
      $estado   = $this->client_model->get_client($data['id_cliente'])['estado'];
			if (!$pendents && $estado != 'mora'){
				if (cancel_contract($this, $data)) {

					$contract = $this->contract_model->get_contract_view($data['id_contrato']);
					$this->event->trigger('contract', 5, $contract, " |Motivo: {$data['motivo']}");
				}
			} else {
				echo MESSAGE_INFO. 'El cliente tiene pagos pendientes, debe hacer el pago antes de cancelar';
			}
		}
	}

	public function data_for_extra() {
		authenticate();
		$dni = $_POST['dni'];
		$dni = str_replace('-','',$dni);
		$data;
		$client = $this->client_model->get_client($dni, true);
		if($client){
			$data['cliente'] = $client;
			$data["contratos"]  = $this->contract_model->get_all_of_client($client->id_cliente,true);
			$dataJson = json_encode($data);
			echo $dataJson;
		}else{
			echo "nada";
		}
	}

	public function extend_contract(){
		authenticate();
		if ($data = $_POST) {
			$this->db->trans_start();
			extend_contract($data, $this);
			$this->db->trans_complete();
			if($this->db->trans_status()){
				echo MESSAGE_SUCCESS." Contrato extendido con exito";

				$contract = $this->contract_model->get_contract_view($data['id_contrato']);
				$this->event->trigger('contract', 7, $contract, "por {$data['duracion']} meses");

			}
			else{
				echo MESSAGE_ERROR."No guardado"." Error";
			}
		}
	}

	public function addextra() {
		authenticate();
		if ($data = $_POST) {
			if ($extra_id = add_extra($this, $data)) {
				$contract_id = $extra_id === true ? $data['id_contrato'] : $extra_id;
				$contract = $extra_id === true ? $this->contract_model->get_contract_view($contract_id) : $this->extra_model->get_extra($extra_id, true);

				switch($data['modo_pago']) {
					case 1:
						$this->event->trigger('contract', 2, $contract, "Agrego a proximo pago servicio {$data['nombre_servicio']}");
					break;
					case 2:
						$this->event->trigger('extra', 1, $contract);
					break;
					case 3:
						$this->event->trigger('contract', 2, $contract, "Nuevo Contrato {$data['nombre_servicio']}");
					break;
					}
			}
		}
	}

	// print documents functions

	public function getrecibo($id) {
		authenticate();
		$recibo_info = $this->payment_model->get_payment($id, true);
		$this->session->set_flashdata('recibo_info', $recibo_info);
		if(str_contain('abono',$recibo_info['concepto'])){
			redirect(base_url("app/imprimir/recibo_abono/$id"));
		}
		redirect(base_url("app/imprimir/recibo/$id"));
	}

	public function getrequirements($id, $type = "cliente", $test= null) {
		authenticate();
		$requirement_info['cliente'] = $this->client_model->get_client($id);
		if($type == "cliente"){
			$contract_id = $this->contract_model->get_last_id_of($id);
		}else{
			$contract_id = $id;
		}

    $requirement_info['contrato'] = $this->contract_model->get_contract_view($contract_id);
    $requirement_info['servicio'] = $this->service_model->get_service($requirement_info['contrato']['id_servicio']);
    if ($requirement_info['contrato']['id_precio']) {
      $precio = $this->price_model->get($requirement_info['contrato']['id_precio']);
      $requirement_info['contrato']['cuota'] = $precio['monto'];
    }

		if(!$requirement_info['cliente'] || $type== "contrato")
			$requirement_info['cliente'] = $this->client_model->get_client($requirement_info['contrato']['id_cliente']);
		  $this->session->set_flashdata('requirement_info',$requirement_info);
		if (!$test) {
			redirect(base_url('app/imprimir/requerimientos'));
		} else {
			var_dump($requirement_info);
		}

	}

	public function getrequirement($client_id,$service_id) {
		authenticate();
		$requirement_info['cliente'] 	= $this->client_model->get_client($client_id);
		$requirement_info['servicio'] = $this->service_model->get_service($service_id);
		$this->session->set_flashdata('requirement_info', $requirement_info);
		redirect(base_url('app/imprimir/requerimiento'));
	}

	public function getcancelcontract($contract_id, $end = false) { // or end of contract
		authenticate();
		$contract = $this->contract_model->get_contract_view($contract_id);
		$requirement_info['contrato'] = $contract;
		$requirement_info['cliente'] 	= $this->client_model->get_client($contract['id_cliente']);
		$requirement_info['pago']	= $this->payment_model->get_last_pay_of($contract_id);
		if (!$end) {
			$requirement_info['cancelacion']	= $this->contract_model->get_cancelation($contract_id);
			$endpoint = 'app/imprimir/cancelacion';
		} else {
			$endpoint = 'app/imprimir/termino';
		}
		$this->session->set_flashdata('requirement_info', $requirement_info);
		redirect(base_url($endpoint));
	}

	public function getreport($table,$type = 'nada') {
		authenticate();

		switch ($table) {
			case 'payment':
					$this->report_model->get_payments_report($type);
				break;
			case 'recibos':
					$this->report_model->get_receipts_report();
					break;
			case 'installations':
					$this->report_model->get_installations(true);
				break;
			case 'deudores':
					$this->report_model->get_moras_view(true);
				break;
			case 'averias':
					$this->report_model->get_averias_report();
				break;
			case 'clientes':
				$type = str_replace('%20',' ',$type);
				$this->report_model->get_client_report($type);
				break;
			case 'secciones':
				$type = str_replace('%20',' ',$type);
				$this->report_model->get_sections_report($type);
				break;
			case 'retiros':
				$this->cancelations_model->print_report();
				break;
			case 'gastos':
			  $this->load->model('caja_mayor');
				$this->caja_mayor->expenses_report();
				break;
			case 'cierres':
			  $this->load->model('caja_mayor');
				$this->caja_mayor->cierres_report();
				break;
		}
			redirect(base_url('app/imprimir/reporte'));

	}

	public function print_page() {
		authenticate();
		$report = $this->contract_view_model->get_technical_report();
		if(!$report) echo "No hay datos";

		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$myreport_sheet = create_excel_file($report, "Contratos Vigentes", "Reporte Tecnico");
    $file = "reporte_tecnico.xlsx";
    download_excel_file($myreport_sheet, $file);
	}

	// helper funtion revisa si el dia ya ha sido cerrado
	public function is_day_closed($mode = 'break') {
		$this->load->model('caja_mayor');
		$last_close_date =$this->caja_mayor->get_last_close_date();
		$today = date('Y-m-d');
		if ($last_close_date == $today){
			echo MESSAGE_INFO.'No puede realizar transacciones luego del cierre de caja';
			return true;
		}
	}

	public function test_update_contract_day($contract_id, $day) {
		$this->payment_model->update_payment_day_contract($contract_id, $day);
	}

}
