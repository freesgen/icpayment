$(function(){
  $('[type="tel"]').inputmask({"mask": "(999) 999-9999",greede: false});
  $('[role="cedula"]').inputmask({"mask": ["999-9999999-9","**-*******-*","************"],greede: false});
  $('[id*="dni"]').inputmask({"mask": ["999-9999999-9","**-*******","*{1,20}"],greede: false});

  window.getVal = function (element){
    return element.inputmask('unmaskedvalue');
  }

  window.isComplete = function(element){
    return element.inputmask('isComplete');
  }
    
    $('input').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
    increaseArea: '20%' // optional
    });
})