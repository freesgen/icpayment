<?php

defined('BASEPATH') or exit('No direct script access allowed');
use freesgen\application\controllers\My_Crud;
use freesgen\application\models\eloquent\Payment;
class Payments extends My_Crud
{
    protected $modelName = 'Payment';
    // protected $listener = PaymentAgreementListener::class;
    protected $model = Payment::class;
    protected $fields = ['fecha', 'codigo'];

    public function index($id = null) {
		$queryParams   = $_SERVER['QUERY_STRING'];
		$queryParams = $this->getParams($queryParams);
		
		if (!$id) {
			$modelQuery = $this->model->select();
			$count = $this->getModelQuery($modelQuery, $queryParams);

			$results = $modelQuery->get()->toArray();

			$returnData = [
					'data' => $results,
					'count' => $count,
			];
		} else {
			$modelQuery = $this->model->find($id);
			$this->getSingleQuery($modelQuery, $queryParams);
			$results = $modelQuery;
			$returnData = [
					'data' => $results,
			];
		}
		
		if ($this->listener) {
			$this->emitter->emit("$this->modelName::afterGet", $results);
		}
        return $this->response_json($returnData);
	}

}