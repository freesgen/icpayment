<?php
/**
* IC Payment
*@author Jesus Guerrero
*@copyright Copyright (c) 2017 Insane Code
*@version 1.0.0
*
*/

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('create_payments')) {
    /**
    * Genera los pagos de un contrato automaticamente
    * @param array $data the result of an select in a query
    * @param int the number for start counting the rows the that is for my custom pagination
    *@return string the tbody with rows of a table
    */

    function create_payments($contract_id, $data, $context)
    {
        $time_zone = new DateTimeZone('America/Santo_Domingo');
        $contract_date = new DateTime($data['fecha']);
        $next_payment_date = $contract_date;
        $duration = $data['duracion'];
        $concepto = 'Instalación';
        $day = $contract_date->format('d');
        $pago = isset($data['instalacion']) ? $data['instalacion'] : 0;

        for ($i = 0; $i < $duration + 1; $i++) {
            if ($i > 0) {
                $concepto = 'mensualidad';
                $pago = $data['mensualidad'];
            }

            $new_data = [
              'id_contrato' => $contract_id,
              'id_servicio' => $data['id_servicio'],
              'id_precio' => isset($data['id_precio']) ? $data['id_precio'] : null,
              'fecha_pago' => null,
              'concepto' => $concepto,
              'cuota' => $pago,
              'mora' => 0,
              'total' => $pago,
              'estado' => 'no pagado',
              'fecha_limite' => $next_payment_date->format('Y-m-d')
            ];

            if ($new_data['total']) {
                $context->payment_model->add($new_data);
            }

            $next_payment_date = get_next_date($next_payment_date, $day);
        }
    }
}

if (!function_exists('refresh_contract')) {
    /**
    * Actualiza los pagos de un contrato automaticamente
    * @param array $data the result of an select in a query
    * @param int the number for start counting the rows the that is for my custom pagination
     *@return string the tbody with rows of a table
    */

    function refresh_contract($contract_id, $context, $data_pago)
    {
        $time_zone = new DateTimeZone('America/Santo_Domingo');

        $contract = $context->contract_model->get_contract_view($contract_id);
        $payment = $context->payment_model->get_payment($data_pago['id']);

        $paymentDay = new DateTime($payment['fecha_limite']);

        $data_contract = [
            'ultimo_pago' => $data_pago['fecha_pago'],
            'dia_pago' => $paymentDay->format('d')
      ];

        $context->contract_model->refresh_contract($data_pago, $data_contract, $contract);
    }
}

if (!function_exists('cancel_payment')) {
    function cancel_payment($payment_id, $context)
    {
        $payment = $context->payment_model->get_payment($payment_id);

        if (!str_contain('abono', $payment['concepto'])) {
            $data_payment = [
                'estado' => 'no pagado',
                'fecha_pago' => null
        ];

            $context->payment_model->update($data_payment, $payment_id);

            if (isset($payment['id_contrato'])) {
                $context->db->where('id_contrato', $payment['id_contrato'])
                ->where('estado', 'pagado')
                ->select('fecha_pago')
                ->order_by('fecha_pago', 'DESC');

                $last_pay_date = $context->db->get('ic_pagos', 1)->row_array()['fecha_pago'];

                $data_contract = [
                    'ultimo_pago' => $last_pay_date,
                    'proximo_pago' => $payment['fecha_limite'],
                    'estado' => 'activo'
                ];

                $contract_debt = $context->contract_model->get_debt_of($payment['id_contrato']);
                $data_contract = array_merge($data_contract, $contract_debt);
                $context->contract_model->update($data_contract, $payment['id_contrato']);
            }

            echo MESSAGE_SUCCESS . ' Pago cancelado';
        } else {
            $contract = $context->contract_model->get_contract_view($payment['id_contrato']);
            cancel_abono($payment, $contract, $context);
        }
    }
}

if (!function_exists('set_abono')) {
    function set_abono($data, $context)
    {
        $date = date('Y-m-d');
        $payment = $context->payment_model->get_next_payment_of($data['contrato_abono']);
        $contract = $context->contract_model->get_contract_view($data['contrato_abono']);

        if ($payment and $data['abonos'] < $payment['cuota'] and $data['abonos'] > 0) {
            $id_empleado = $_SESSION['user_data']['user_id'];
            $to_pay = new DATETIME($payment['fecha_limite']);
            $to_pay = str_replace($GLOBALS['full_months_eng'], $GLOBALS['full_months_esp'], $to_pay->format('F'));

            $data_abono = [
        'id_contrato' => $data['contrato_abono'],
        'id_servicio' => $payment['id_servicio'],
        'fecha_pago' => $date,
        'concepto' => "abono ($to_pay)",
        'detalles_extra' => $data['observaciones'],
        'cuota' => $data['abonos'],
        'mora' => 0,
        'total' => $data['abonos'],
        'estado' => 'pagado',
        'id_empleado' => $id_empleado,
        'fecha_limite' => $date,
        'deuda' => $payment['total'] - $data['abonos'],
        'abono_a' => $payment['id_pago']
      ];

            $id_abono = $context->payment_model->add($data_abono);
            $new_cuota = $payment['cuota'] - $data['abonos'];
            $updated_payment = [
        'cuota' => $new_cuota,
        'total' => $payment['mora'] + $payment['monto_extra'] + $new_cuota
      ];

            $context->payment_model->update($updated_payment, $payment['id_pago']);

            $data_contract = ['ultimo_pago' => $date];
            $contract_debt = $context->contract_model->get_debt_of($contract['id_contrato']);
            $data_contract = array_merge($data_contract, $contract_debt);
            $data_contract['estado'] = $context->contract_model->get_status_for($contract, $contract_debt);

            $context->contract_model->update($data_contract, $contract['id_contrato']);

            echo MESSAGE_SUCCESS . ' El abono ha sido registrado correctamente';
            return $id_abono;
        } else {
            echo MESSAGE_INFO . ' El abono no puede ser mayor a la cuota del pago';
        }
    }
}

if (!function_exists('cancel_abono')) {
    function cancel_abono($abono, $contract, $context)
    {
        $abono_owner = $context->payment_model->get_payment($abono['abono_a']);
        $total = $abono['total'];

        if ($abono_owner['estado'] != 'pagado') {
            $context->db->query('delete from ic_pagos where id_pago=' . $abono['id_pago']);
            $last_payment = $context->payment_model->get_last_pay_of($abono['id_contrato']);

            $new_cuota = $abono_owner['cuota'] + $total;
            $updated_payment = [
        'cuota' => $new_cuota,
        'total' => $abono_owner['mora'] + $abono_owner['monto_extra'] + $new_cuota
      ];

            $context->payment_model->update($updated_payment, $abono_owner['id_pago']);

            $data_contract = ['ultimo_pago' => $last_payment['fecha_pago']];
            $contract_debt = $context->contract_model->get_debt_of($contract['id_contrato']);
            $data_contract = array_merge($data_contract, $contract_debt);
            $data_contract['estado'] = $context->contract_model->get_status_for($contract, $contract_debt);

            $context->contract_model->update($data_contract, $contract['id_contrato']);
            echo MESSAGE_SUCCESS . ' Pago eliminado';
        } else {
            echo MESSAGE_INFO . ' El pago al que pertenecia este abono se realizó, este abono ya no puede ser eliminado';
        }
    }
}

if (!function_exists('payments_up_to_date')) {
    function payments_up_to_date($data)
    {
        $context = &get_instance();
        $contract_id = $data['id_contrato'];
        $contract = $context->contract_model->get_contract_view($contract_id);
        $payments = $context->payment_model->get_payments_of_contract($contract_id);
        $new_payment;
        $last_date;
        $state;
        $count = 0;

        clear_payments($data);

        if ($payments) {
            $id_empleado = $_SESSION['user_data']['user_id'];
            $acum = 0;
            foreach ($payments as $payment) {
                if ($payment['id_pago'] > $data['id_ultimo_pago']) {
                    break;
                }
                $last_date = $payment['fecha_limite'];
                $new_payment = [
          'id_empleado' => $id_empleado,
          'estado' => 'pagado',
          'fecha_pago' => $last_date,
          'complete_date' => date('Y-m-d H:i:s')
        ];
                $context->payment_model->update($new_payment, $payment['id_pago']);
                $count++;
                $acum += $payment['cuota'];
            }

            $monto_pagado = $acum;
            if ($monto_pagado == $contract['monto_total']) {
                $state = 'activo';
            } else {
                $state = 'activo';
            }

            $data_contract = [
        'monto_pagado' => $monto_pagado,
        'ultimo_pago' => $last_date,
        'estado' => $state
      ];
            $context->contract_model->update($data_contract, $contract_id, false);
        }
    }
}

if (!function_exists('clear_payments')) {
    function clear_payments($data)
    {
        $context = &get_instance();
        $contract_id = $data['id_contrato'];
        $contract = $context->contract_model->get_contract_view($contract_id);
        $payments = $context->payment_model->get_payments_of_contract($contract_id);
        $new_payment;
        $state;

        $id_empleado = $_SESSION['user_data']['user_id'];
        foreach ($payments as $payment) {
            $new_payment = [
        'id_empleado' => null,
        'estado' => 'no pagado',
        'fecha_pago' => null,
        'complete_date' => null
      ];
            $context->payment_model->update($new_payment, $payment['id_pago']);
        }

        $monto_pagado = 0.00;
        if ($monto_pagado == $contract['monto_total']) {
            $state = 'activo';
        } else {
            $state = 'activo';
        }

        $data_contract = [
      'monto_pagado' => 0.00,
      'ultimo_pago' => null,
      'estado' => $state
    ];
        $context->contract_model->update($data_contract, $contract_id, true);
    }
}

if (!function_exists('upgrade_contract')) {
    /**
    * Actualiza los pagos de un contrato automaticamente
    * @param array $data the result of an select in a query
    * @param int the number for start counting the rows the that is for my custom pagination
    *@return string the tbody with rows of a table
    */

    function upgrade_contract($context, $data_change)
    {
        $contract_id = $data_change['id_contrato'];
        $contract = $context->contract_model->get_contract_view($contract_id);
        $unpaid_payments = $context->payment_model->count_unpaid_per_contract($contract_id, true);

        $monto_total = $contract['monto_pagado'] + ($data_change['cuota'] * $unpaid_payments);

        $data_contract = [
        'id_contrato' => $contract_id,
        'monto_total' => $monto_total,
        'id_servicio' => $data_change['id_servicio'],
        'id_precio' => $data_change['id_precio']
        ];

        $data_payment = [
        'id_contrato' => $contract_id,
        'id_servicio' => $data_change['id_servicio'],
        'id_precio' => $data_change['id_precio'],
        'cuota' => $data_change['cuota'],
        'monto_total' => $data_change['cuota']
        ];

        if ($data_change['id_servicio'] != $contract['id_servicio'] || $data_change['id_precio'] != $contract['id_precio']) {
            return $context->contract_model->upgrade_contract($data_payment, $data_contract);
        } else {
            echo MESSAGE_INFO . ' Es el mismo servicio';
            return 'same';
        }
    }
}

if (!function_exists('update_contract_from_service')) {
    /**
    * Actualiza los pagos de un contrato automaticamente
    * @param array $data the result of an select in a query
    * @param int the number for start counting the rows the that is for my custom pagination
    *@return string the tbody with rows of a table
    */

    function update_contract_from_service($data_change)
    {
        $ci = &get_instance();
        $service_id = $data_change['id_servicio'];
        $contracts = $ci->contract_view_model->get_contract_view_of_service($service_id);
        $count = 0;
        $contratos_a_cambiar = count($contracts);

        foreach ($contracts as $contract) {
            if ($contract['id_precio'] == 0 || $contract['id_precio'] == null) {
                $contract_id = $contract['id_contrato'];
                $pagos_restantes = $ci->payment_model->count_unpaid_per_contract($contract_id);
                $monto_total = $contract['monto_pagado'] + ($data_change['mensualidad'] * $pagos_restantes);

                $data_contract = [
          'monto_total' => $monto_total,
        ];

                $ci->db->where('id_contrato', $contract_id);
                $payments = $ci->payment_model->get_unpaid_per_contract($contract_id);

                foreach ($payments as $payment) {
                    $total = $data_change['mensualidad'] + $payment['mora'] + $payment['monto_extra'];

                    $data_pago = [
            'cuota' => $data_change['mensualidad'],
            'total' => $total
          ];

                    $ci->db->where('id_pago', $payment['id_pago']);
                    $ci->db->update('ic_pagos', $data_pago);
                }
                $ci->db->where('id_contrato', $contract['id_contrato']);
                $ci->db->update('ic_contratos', $data_contract);
                $count++;
            }
        }

        echo ' ' . $count . ' de ' . $contratos_a_cambiar . ' contratos actualizados';
        return true;
    }
}

function update_contract_from_price($data_change)
{
    $ci = &get_instance();
    $price_id = $data_change['id_precio'];
    $contracts = $ci->contract_view_model->get_contract_view_of('id_precio', $price_id);
    $count = 0;
    $contratos_a_cambiar = count($contracts);

    foreach ($contracts as $contract) {
        $contract_id = $contract['id_contrato'];
        $pagos_restantes = $ci->payment_model->count_unpaid_per_contract($contract_id);
        $monto_total = $contract['monto_pagado'] + ($data_change['monto'] * $pagos_restantes);

        $data_contract = [
      'monto_total' => $monto_total,
    ];

        $ci->db->where('id_contrato', $contract_id);
        $payments = $ci->payment_model->get_unpaid_per_contract($contract_id);

        foreach ($payments as $payment) {
            $total = $data_change['monto'] + $payment['mora'] + $payment['monto_extra'];

            $data_pago = [
        'cuota' => $data_change['monto'],
        'total' => $total
      ];

            $ci->db->where('id_pago', $payment['id_pago']);
            $ci->db->update('ic_pagos', $data_pago);
        }
        $ci->db->where('id_contrato', $contract['id_contrato']);
        $ci->db->update('ic_contratos', $data_contract);

        $count++;
    }

    echo ' ' . $count . ' de ' . $contratos_a_cambiar . ' contratos actualizados';
    return true;
}

function payment_discount($data, $context)
{
    $data_pago = [
    'id' => $data['id_pago'],
    'estado' => 'pagado',
    'fecha_pago' => $data['fecha_pago'],
    'id_contrato' => $data['id_contrato']
  ];

    $data_discount = [
    'cuota' => $data['cuota'],
    'mora' => $data['mora'],
    'monto_extra' => $data['monto_extra'],
    'total' => $data['total'],
    'descuento' => $data['descuento'],
    'detalles_extra' => $data['detalles_extra'],
    'razon_descuento' => $data['razon_descuento']
    ];

    $context->payment_model->update($data_discount, $data['id_pago']);

    $context->db->where('id_contrato', $data['id_contrato']);
    $context->db->select_sum('cuota');
    $suma = $context->db->get('ic_pagos');
    $suma = $suma->row_array()['cuota'];
    $context->db->where('id_contrato', $data['id_contrato']);
    $context->db->update('ic_contratos', ['monto_total' => $suma]);

    refresh_contract($data['id_contrato'], $context, $data_pago);
}

function extend_contract($data, $context)
{
    $contract_id = $data['id_contrato'];
    if (!$context) {
        $context  =& get_instance();
    }

    $last_payment = $context->payment_model->get_last_pay_of($contract_id);
    $next_payment_date = new DateTime($last_payment['fecha_limite']);
    $day = $next_payment_date->format('d');
    $next_payment_date = get_next_date($next_payment_date, $day);
    $contract = $context->contract_model->get_contract_view($contract_id);
    $num_pago = $context->payment_model->count_of_contract($contract_id);
    $duration = $num_pago + $data['duracion'] - 1;

    $new_data = [
      'duracion' => $duration,
      'monto_total' => $contract['monto_total'] + ($data['duracion'] * $contract['cuota']),
      'estado' => 'activo'
    ];

    $is_saved = $context->contract_model->update($new_data, $contract_id);
    if ($is_saved) {
        for ($i = $num_pago; $i <= $duration; $i++) {
            if ($i > 0) {
                $concepto = 'mensualidad';
            }

            $new_data = [
                'id_contrato' => $contract_id,
                'id_servicio' => $contract['id_servicio'],
                'fecha_pago' => null,
                'concepto' => $concepto,
                'cuota' => $contract['cuota'],
                'mora' => 0,
                'total' => $contract['cuota'],
                'estado' => 'no pagado',
                'fecha_limite' => $next_payment_date->format('Y-m-d')
            ];


            if ($paymentId = $context->payment_model->add($new_data)) {
                $next_payment_date = get_next_date($next_payment_date, $day);
            }
        }
    }
}

function reconnect_contract($data, $context)
{
    // {id_contrato, fecha, id_servicio, duracon, ip}
    $contract = $context->contract_model->get_contract_view($data['id_contrato']);
    $service = $context->service_model->get_service($data['id_servicio']);
    $duration = $contract['duracion'] + $data['duracion'];

    $payment_data = [
        'mensualidad' => $service['mensualidad'],
        'duracion' => $data['duracion'],
        'id_servicio' => $data['id_servicio'],
        'fecha' => $data['fecha']
  ];

    create_payments($data['id_contrato'], $payment_data, $context);
    $context->client_model->update(['estado' => 'activo', 'id' => $contract['id_cliente']], false);
    $monto_total = $context->payment_model->get_sum_monto_total_of($data['id_contrato']);

    $new_data_contract = [
        'duracion' => $duration,
        'monto_total' => $monto_total,
        'estado' => 'activo',
        'id_servicio' => $data['id_servicio'],
        'estado_instalacion' => 'por instalar',
  ];

    $context->contract_model->update($new_data_contract, $data['id_contrato']);
}

if (!function_exists('cancel_contract')) {
    function cancel_contract($context, $data_cancel, $message = true, $include_payment = true)
    {
        $id_empleado = $_SESSION['user_data']['user_id'];
        $contract_id = $data_cancel['id_contrato'];
        $contract = $context->contract_model->get_contract_view($contract_id);
        $settings = $context->settings_model->get_settings();
        if ($data_cancel['penalidad'] == 'true') {
            $penalizacion = ($settings['penalizacion_cancelacion'] / 100) * ($contract['cuota'] * 12);
        } else {
            $penalizacion = 0;
        }

        $monto_total = $contract['monto_pagado'] + $penalizacion;

        $data_contract = [
            'id_contrato' => $contract_id,
            'monto_total' => $monto_total,
            'monto_pagado' => $monto_total,
            'proximo_pago' => null,
            'ultimo_pago' => $data_cancel['fecha']
        ];

        $data_pago = null;
        $data_cancel_to_save = null;

        if ($include_payment) {
            $data_pago = [
                'id_contrato' => $data_cancel['id_contrato'],
                'id_empleado' => $id_empleado,
                'id_servicio' => $contract['id_servicio'],
                'fecha_pago' => $data_cancel['fecha'],
                'concepto' => 'Cancelación de Contrato',
                'cuota' => $penalizacion,
                'mora' => 0,
                'total' => $penalizacion,
                'estado' => 'pagado',
                'fecha_limite' => $data_cancel['fecha']
            ];

            $data_cancel_to_save = [
                'id_contrato' => $data_cancel['id_contrato'],
                'motivo' => $data_cancel['motivo']
            ];
        }

        return $context->contract_model->cancel_contract($data_pago, $data_contract, $contract, $data_cancel_to_save, $message);
    }
}

if (!function_exists('add_extra')) {
    function add_extra($context, $data_extra)
    {
        $contract_id = $data_extra['id_contrato'];
        $contract = $context->contract_model->get_contract_view($contract_id);
        $extra_id = '';

        switch ($data_extra['modo_pago']) {
      case 1:
        $next_payment = $context->payment_model->get_next_payment_of($contract_id);
        $detalles_extra = $next_payment['detalles_extra'] . ' - ' . $data_extra['nombre_servicio'];
        $monto_extra = $next_payment['monto_extra'] + $data_extra['costo_servicio'];
        $total = $next_payment['cuota'] + $next_payment['mora'] + $monto_extra;

        $data_contract = [
          'router' => $data_extra['router'],
          'mac_router' => $data_extra['mac_router'],
          'nombre_equipo' => $data_extra['nombre_equipo'],
          'mac_equipo' => $data_extra['mac_router']
        ];

        $data_pago = [
          'detalles_extra' => $detalles_extra,
          'monto_extra' => $monto_extra,
          'total' => $total
        ];

        $context->contract_model->add_extra_service($data_contract, $contract_id, $data_pago, $next_payment['id_pago']);
        break;

      case 2:
        $id_empleado = $_SESSION['user_data']['user_id'];
        $service = $context->service_model->get_service($data_extra['nombre_servicio']);

        $new_extra = [
          'id_cliente' => $contract['id_cliente'],
          'id_servicio' => $service['id_servicio'],
          'id_empleado' => $id_empleado,
          'servicio' => $data_extra['nombre_servicio'],
          'fecha' => date('Y-m-d'),
          'monto_pagado' => 0,
          'ultimo_pago' => '',
          'deuda' => $data_extra['costo_servicio'],
          'monto_total' => $data_extra['costo_servicio']
        ];
        $extra_id = $context->extra_model->add_extra($new_extra);
        break;

      case 3:
        $service = $context->service_model->get_service($data_extra['nombre_servicio']);
        $context->contract_model->update(['extras_fijos' => $service['id_servicio']], $contract_id);
        echo MESSAGE_SUCCESS . ' Seguro Agregado';
        break;

      case 4:
        $service = $context->service_model->get_service($data_extra['nombre_servicio']);
        if ($context->aditional_service_model->add($service['id_servicio'], $contract_id)) {
            echo MESSAGE_SUCCESS . 'Extra Fijo Agregado';
        } else {
            echo MESSAGE_ERROR . 'No pudo agregarse el extra';
        }
        break;
        }
        return $extra_id ? $extra_id : true;
    }
}

// Suspensions
function suspender_contrato($id_contrato, $id_cliente, $context)
{
    generar_facturas_contrato($id_contrato, $context);
    $date = date('Y-m-d');

    $context->db->trans_start();
    $context->contract_model->update(['estado' => 'suspendido'], $id_contrato);
    $context->db->where('id_cliente', $id_cliente);
    $context->db->update('ic_clientes', ['estado' => 'suspendido', 'suspension_date' => $date]);

    //borrando los pagos y actualizando la deuda
    $context->db->where('estado ="no pagado" and generado = false')
  ->where('id_contrato', $id_contrato)
  ->delete('ic_pagos');

    $suma = $context->db->where('id_contrato', $id_contrato)
          ->select_sum('cuota')
          ->get('ic_pagos')->row_array()['cuota'];

    $context->contract_model->update(['monto_total' => $suma], $id_contrato);
    $context->db->trans_complete();

    if ($context->db->trans_status() === false) {
        $context->db->trans_rollback();
        return false;
    } else {
        return true;
    }
}

function generar_facturas_contrato($id_contrato, $context)
{
    $context->db->where('contrato', $id_contrato);
    $contrato = $context->db->get('v_pagos_generados')->row_array();

    $context->db->select('id_pago,estado');
    $pagos = $context->db->where('id_contrato', $contrato['contrato'])
  ->where('fecha_limite < current_date()')
  ->get('ic_pagos')->result_array();

    foreach ($pagos as $pago) {
        if ($contrato['pagos_generados'] < 3) {
            $context->payment_model->update(['generado' => true], $pago['id_pago']);
            if ($pago['estado'] == 'no pagado') {
                $contrato['pagos_generados'] += 1;
            }
        }
    }
}

// dates helper functions

function get_next_date($date, $payment_day)
{
    switch ($date->format('m')) {
      case '01':
          $date = getForFebruary($date);
          break;
      case '02':
          $date = getForMarch($date, $payment_day);
          break;
      default:
          $date->add(findNextDate($date));
          break;
    }
    return $date;
}

function getForFebruary($date)
{
    $year = $date->format('Y');
    $month = '02';
    $day = $date->format('d');
    if ($day > 28) {
        $day = '28';
    }
    $newdate = "$year-$month-$day";
    return new DateTime($newdate);
}

function findNextDate($date)
{
    $lastDateOfNextMonth = new DateTime($date->format('Y-m-d'));
    $lastDateOfNextMonth->modify('last day of +1 month');
    if ($date->format('d') > $lastDateOfNextMonth->format('d')) {
        return $date->diff($lastDateOfNextMonth);
    } else {
        return new DateInterval('P1M');
    }
}

function getForMarch($date, $payment_day)
{
    $year = $date->format('Y');
    $month = '03';
    $day = $date->format('d');
    if ($day == 28) {
        $day = $payment_day;
    }
    $newdate = "$year-$month-$day";
    return new DateTime($newdate);
}

function get_cuota($id_contrato, $context)
{
    $context->db->where('id_contrato', $id_contrato)
  ->select('cuota');
    $cuota = $context->db->get('v_contratos', 1)->row_array()['cuota'];
    return $cuota;
}

function get_settings()
{
    $ci = &get_instance();
    $settings = $ci->db->get('ic_settings', 1);
    return $settings->row_array();
}
