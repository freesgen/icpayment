<?php
namespace freesgen\application\listeners;
use League\Event\AbstractListener;
use League\Event\EventInterface;
use freesgen\application\libraries\MyPusher;

class ListenerBase extends AbstractListener
{
    protected $listenerName;

    public function handle(EventInterface $event, $param = null)
    {
        $this->pusher = new MyPusher();
        $eventName = $event->getName();
        $eventName = str_replace("$this->listenerName::", "", $eventName);
        $this->$eventName($event, $param);
    }

    public function beforeGet(EventInterface $event, $param = null) {
        
    }
    public function afterGet(EventInterface $event, $param = null) {

    }

    public function beforeCreate(EventInterface $event, $param = null) {
        
    }

    public function afterCreate(EventInterface $event, $param = null) {
        
    }

    public function beforeUpdate (EventInterface $event, $param = null) {
        
    }

    public function afterUpdate(EventInterface $event, $param = null) {
        
    }

    public function beforeDelete(EventInterface $event, $param = null) {
        
    }

    public function afterDelete(EventInterface $event, $param = null) {
        
    }
}