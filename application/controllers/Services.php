<?php

defined('BASEPATH') or exit('No direct script access allowed');
use freesgen\application\controllers\My_Crud;
use freesgen\application\models\eloquent\Service;

class Services extends My_Crud
{
    protected $modelName = 'Service';
    // protected $listener = PaymentAgreementListener::class;
    protected $model = Service::class;
    protected $fields = ['fecha', 'codigo'];
}