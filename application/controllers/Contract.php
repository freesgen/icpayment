<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contract extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contract_model');
        $this->load->model('service_model');
        $this->load->model('settings_model');
        $this->load->model('client_model');
        $this->load->model('cancelations_model');
    }

    public function suspend()
    {
        authenticate();
        if ($data   = $this->get_post_data('data')) {
            $contract = $this->contract_model->get_contract_view($data['id_contrato']);

            if (suspender_contrato($data['id_contrato'], $contract['id_cliente'], $this)) {
                $res['mensaje'] = MESSAGE_SUCCESS." Contrato suspendido";
                $this->event->trigger('contract', 6, $contract);
            } else {
                $res['mensaje'] = MESSAGE_ERROR." El contrato no pudo ser suspendido";
            }

            $this->response_json($res);
        }
    }

    public function reconnect()
    {
        authenticate();
        $data = json_decode($_POST['data'], true);
        $this->db->where("id_contrato", $data['id_contrato'])
                            ->where('fecha_limite', $data['fecha']);
        $number = $this->db->count_all_results('ic_pagos');

        if ($number == 0) {
            $this->db->trans_start();
            reconnect_contract($data, $this);
            $this->db->trans_complete();

            if ($this->db->trans_status() === false) {
                $res['mensaje']	= MESSAGE_ERROR. " El contrato/cliente no pudo ser reconectado";
            } else {
                $res['mensaje'] = MESSAGE_SUCCESS." El contrato/cliente ha sido reconectado";
                $this->contract_model->delete_cancelation($data['id_contrato']);
            }
        } else {
            $res['mensaje'] = MESSAGE_INFO." ya hay pagos para esta fecha";
        }
        echo json_encode($res);
    }

    public function getCancelations()
    {
        authenticate();
        $data = json_decode($_POST['data'], true);
        if ($data) {
            $res['content'] = $this->cancelations_model->get_cancelations($data['first_date'], $data['second_date']);
            echo json_encode($res);
        }
    }

    public function delete_extra()
    {
        authenticate();
        $data = $this->get_post_data('data');
        if ($data) {
            $contract = $this->contract_model->get_contract_view($data['id_contrato']);
            $service = $this->service_model->get_service($contract['extras_fijos']);

            $res['mensaje'] = MESSAGE_ERROR . " Error al eliminar servicio adicional";
            if ($this->contract_model->update(['extras_fijos' => null], $data['id_contrato'])) {
                $res['mensaje'] = MESSAGE_SUCCESS . " Extra eliminado con exito";

                $this->event->trigger('contract', 2, $contract, "Elimino Extra Fijo: {$service['nombre']}");
            }
            echo json_encode($res);
        }
    }

    public function get_contracts($client_id, $type = 'html')
    {
        $contractList = $this->contract_model->get_contracts_dropdown($client_id, $type);
        if ($contractList) {
            echo json_encode($contractList);
        }
    }
}