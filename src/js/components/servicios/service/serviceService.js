import axios from 'axios';
import utils from './../../sharedComponents/utils';

const $http = axios.create({
  baseURL
});

export default class serviceService {
  constructor() {
    this.$http = $http;
  }

  getServiceList(type = '') {
    return this.$http.post(`services?relationships=precios&byColumn=0&query=${type}&filters=tipo&orderBy=mensualidad&ascending=1`);
  }

  getService(id) {
    return this.$http.post('api/v2/service/get_service', utils.getDataForm({ id }))
      .then(res => res.data.service);
  }
}
